﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DevOpsConsole
{
    /// <summary>
    /// Superclass AzureServer. Inherited by Council. 
    /// Holds core server connection strings and 
    /// database methods RunQuery, RunNonQuery, RunScalarQuery. 
    /// </summary>
    public class AzureServer
    {
        protected Dictionary<string, string> Databases = new Dictionary<string, string>();

        // Parametered Constructor for standalone usage
        public AzureServer(Dictionary<string, string> Databases)
        {
            this.Databases = Databases;
        }
        public string serverString;

        // Parameterless Constructor for Inheritance
        public AzureServer() { }

        private string deriveConnectionString(string Database, bool customDatabase)
        {
            // If the Database name has been manually specified
            if (customDatabase == true)
                return serverString.Replace("##Catalog##", Database);

            // Else derive the Database Name from the DatabaseFields dictionary
            if (Databases.ContainsKey(Database.ToLower()))
                return serverString.Replace("##Catalog##", Databases[Database.ToLower()]);

            throw new ArgumentException("Council's dictionary has no such Database: ' " + Database + "'. Parameter given is not a valid key of this class's DatabaseFields dictionary");
        }

        private DataTable fillTable(SqlCommand queryString, string connectionString)
        {
            queryString.CommandTimeout = 180;
            SqlConnection myConnection = new SqlConnection(connectionString);

            // Declare a DataTable to store the result of the query
            DataTable table = new DataTable();

            // Open the connection and run the query
            // Let this part Except if query is invalid - will need stack trace
            queryString.Connection = myConnection;
            myConnection.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(queryString);
            adapter.Fill(table);
            myConnection.Close();

            return table;
        }

        /// <summary>
        /// Runs an SQL Query against a database in the class' Database dictionary.
        /// Returns the result in a DataTable object.
        /// Queries that don't return any results will yield a null object.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        ///<param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        /// <returns></returns>
        public DataTable RunQuery(SqlCommand query, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (query == null) { return null; }

            DataTable results = fillTable(query, deriveConnectionString(Database, customDatabase));

            // If the query returned no rows return null 
            // instead of an empty DataTable which would make debugging harder
            if ((results == null) || (results.Rows.Count == 0)) { return null; }
            else { return (results); }
        }

        /// <summary>
        /// Runs an SQL Query against a database in the class' Database dictionary.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        /// <param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        public void RunNonQuery(SqlCommand queryString, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (queryString == null) { return; }

            // Search the Council's database dictionary for a 
            // match with the Database parameter
            SqlConnection myConnection = new SqlConnection(deriveConnectionString(Database, customDatabase));
            queryString.CommandTimeout = 180;

            // Open the connection and run the query
            // Let this part Except if query is invalid - will need stack trace
            queryString.Connection = myConnection;
            myConnection.Open();
            queryString.ExecuteNonQuery();
            myConnection.Close();
        }

        /// <summary>
        /// Runs a Scalar SQL Query against a database in the class' Database dictionary.
        /// Returns the result as a string. Excepts if query produces tabular results.
        /// Returns null if the query returns no result.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        /// <param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        /// <returns></returns>
        public string RunScalarQuery(SqlCommand query, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (query == null)
            {
                return null;
            }

            DataTable results = fillTable(query, deriveConnectionString(Database, customDatabase));

            // If the query returned no rows return null 
            // instead of an empty DataTable which would make debugging harder
            // If the query returned more than one throw an exception
            // Else return the first value of the first row 
            if ((results == null) || (results.Rows.Count == 0)) { return null; }
            else if (results.Rows.Count > 1 || results.Columns.Count > 1)
                throw new ArgumentException("Query returned more than one result. Use RunQuery for non-scalar queries, else re-factor query.");
            else if (results.Rows[0][0] == null) { return null; }
            else { return results.Rows[0][0].ToString(); }
        }

        /// <summary>
        /// Runs an SQL Query against a database in the class' Database dictionary.
        /// Returns the results as a list of strings.
        /// The query MUST select a single column.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        ///<param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        /// <returns></returns>
        public List<string> RunListQuery(SqlCommand query, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (query == null) { return null; }

            DataTable results = fillTable(query, deriveConnectionString(Database, customDatabase));

            // If the query returned no rows return null 
            // instead of an empty DataTable which would make debugging harder
            if ((results == null) || (results.Rows.Count == 0)) { return null; }

            if (results.Columns.Count != 1) { throw new ArgumentException("Query returned more than one column. Use RunQuery to get tabular results."); }

            List<string> columnToReturn = new List<string>();
            foreach (DataRow row in results.Rows)
                columnToReturn.Add(row[0].ToString());

            return columnToReturn;
        }
    }
}