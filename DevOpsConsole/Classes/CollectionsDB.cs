﻿using DevOpsConsole.ScheduledTasks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DevOpsConsole
{
    /// <summary>
    /// Stores information and methods about a Client.
    /// Inherits from AzureServer which handles the core database querying methods.
    /// </summary>
    public class CollectionsDB : AzureServer
    {
        #region Attributes and Instantiators
        /// <summary>
        /// The pretty name e.g. "Oadby and Wigston"
        /// </summary>
        public readonly string displayName;
        /// <summary>
        /// The database name e.g. "oadbyandwigston"
        /// </summary>
        public readonly string URLName;

        /// <summary>
        /// The "third format" used by WM Design and the OLD Users tables
        /// e.g. "WestLancs" instead of "westlancs" or "West Lancashire"
        /// </summary>
        public readonly string oldUserTableName;

        // URLName used by AzureMonitoring, to be factored out soon
        public string Name;
        // Status used by AzureMonitoring
        public string Status;


        /// <summary>
        /// List of domains client e-mails may have. 
        /// Only e-mails ending in these domains appear in User Admin.
        /// </summary>
        public List<string> associatedDomains;

        /// <summary>
        /// E-mails that may appear to the user in User Admin.
        /// </summary>
        public List<string> clientsideEmails;

        /// <summary>
        /// Default constructor - requires the urlName of the client
        /// </summary>
        /// <param name="urlName"></param>
        public CollectionsDB(string urlName)
        {
            DataTable councilTable = getClientInfo(urlName);

            if (councilTable == null)
                throw new ArgumentException(urlName + " was not a valid council URLName, or it does not have a Collections database.");


            displayName = councilTable.Rows[0]["Name"].ToString();
            URLName = councilTable.Rows[0]["URLname"].ToString();
            oldUserTableName = councilTable.Rows[0]["OldUserTableName"].ToString();
            Status = councilTable.Rows[0]["Collections-Live"].ToString();

            // Inherited from AzureServer
            Databases = new Dictionary<string, string>();
            Databases.Add("collections", councilTable.Rows[0]["CollectionsDBName"].ToString());
            Databases.Add("incab", councilTable.Rows[0]["InCabDBName"].ToString());
            serverString = "Data Source=" + councilTable.Rows[0]["serverString"].ToString() + ".database.windows.net;" +
"Initial Catalog=##Catalog##;" +
"User ID=mike.devine@webaspx.com@" + councilTable.Rows[0]["serverString"].ToString() + ";" +
"password=Web08#580;";

            this.Name = Regex.Replace(displayName, @"\s+", "");
            if (this.Name.ToLower() == "westlancashire")
                this.Name = "WestLancs";

            //this.Status = Status;
            if (Status == "True")
                this.Status = "Live";
            else
                this.Status = "NotLive";
        }

        /// <summary>
        /// Retrieve this client's record from the ClientInfo table on WebaspxMaster
        /// </summary>
        /// <param name="urlName"></param>
        /// <returns></returns>
        public DataTable getClientInfo(string urlName)
        {
            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT * FROM ClientInfo JOIN AutomationInfo ON ClientInfo.URLname = AutomationInfo.URLname WHERE ClientInfo.URLname = @urlname AND CollectionsDBName IS NOT NULL AND CollectionsDBName != ''", councilGetter);
            getCouncils.Parameters.Add(new SqlParameter("urlname", urlName));
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            return councilTable;
        }
        #endregion


        /// <summary>
        /// Overwrites values of Day_DDD, Week, ServiceName and Crew with those split from the RoundName. 
        /// </summary>
        /// <param name="Roundname"></param>
        public void setComponentsToRoundname(string Roundname)
        {
            DBValidator validator = new DBValidator();
            List<string> Components = validator.getRoundnameComponents(Roundname);

            SqlCommand CorrectComponents = new SqlCommand("UPDATE WaxRoundNew SET Day_DDD = @Day, Week = @Week, " +
                "ServiceName = @Service, Crew = @Crew WHERE RoundName = @Roundname");
            CorrectComponents.Parameters.Add(new SqlParameter("Crew", Components[0]));
            CorrectComponents.Parameters.Add(new SqlParameter("Day", Components[1]));
            CorrectComponents.Parameters.Add(new SqlParameter("Week", Components[2]));
            CorrectComponents.Parameters.Add(new SqlParameter("Service", Components[3]));
            CorrectComponents.Parameters.Add(new SqlParameter("Roundname", Roundname));

            RunNonQuery(CorrectComponents, "Collections");
        }

        /// <summary>
        /// Remove all duplicate Rounds or Bins from the RoundShop and BinShop
        /// </summary>
        public void RemoveShopDuplicates()
        {
            RunQuery(new SqlCommand(File.ReadAllText(@"Queries\RemoveRoundShopDuplicates.sql")), "Collections");
            RunQuery(new SqlCommand(File.ReadAllText(@"Queries\RemoveBinShopDuplicates.sql")), "Collections");
        }

        /// <summary>
        /// Adds any rounds that exist in WaxRoundNew but not RoundShop to the RoundShop
        /// </summary>
        public void UpdateRoundshop()
        {
            // Get all unique roundnames from WaxRoundNew
            DataTable WaxRoundNew = RunQuery(new SqlCommand(File.ReadAllText(@"Queries\RoundsNotInShop.sql")), "Collections");
            if (WaxRoundNew == null)
                return;

            DBValidator validator = new DBValidator();
            // Get a list of valid roundnames from the above
            List<string> Roundnames = new List<string>();
            foreach (DataRow row in WaxRoundNew.Rows)
            {
                if (validator.validateRoundname(row["RoundName"].ToString()))
                    Roundnames.Add(row["RoundName"].ToString());
            }

            // 

            // Go through the list of Correct, Missing roundnames to be added
            foreach (string Roundname in Roundnames)
            {
                string Service = validator.getRoundnameComponents(Roundname)[3];
                Service = ServiceToRoundshopFormat(Service).ToUpper();

                SqlCommand GetHighestOrderForService = new SqlCommand("SELECT MAX(DisplayOrder) FROM RoundShop WHERE Service = @Service");
                GetHighestOrderForService.Parameters.Add(new SqlParameter("Service", Service));

                int DisplayOrder = -1;
                string x = RunScalarQuery(GetHighestOrderForService, "Collections");
                int.TryParse(x, out DisplayOrder);

                if (DisplayOrder < 0)
                    throw new InvalidDataException("Didn't get a DisplayOrder");

                SqlCommand InsertRound = new SqlCommand("INSERT INTO RoundShop " +
                    "(RoundName, Service, DisplayOrder, Show) " +
                    "VALUES " +
                    "(@RoundName, @Service, @DisplayOrder, @Show);");
                InsertRound.Parameters.Add(new SqlParameter("RoundName", Roundname));
                InsertRound.Parameters.Add(new SqlParameter("Service", Service));
                InsertRound.Parameters.Add(new SqlParameter("DisplayOrder", DisplayOrder + 3));
                InsertRound.Parameters.Add(new SqlParameter("Show", "Y"));

                RunNonQuery(InsertRound, "Collections");
            }
        }

        /// <summary>
        /// Converts a service name from Collections format to its InCab equivalent.
        /// </summary>
        /// <param name="Service"></param>
        /// <returns></returns>
        private string ServiceToRoundshopFormat(string Service)
        {
            if (Service == "Ref") { return "Refuse"; }
            if (Service == "Rec") { return "Recycling"; }
            if (Service == "Grn") { return "Garden"; }
            if (Service == "Tra") { return "Trade"; }
            if (Service == "Trd") { return "Trade"; }
            if (Service == "Foo") { return "Food"; }
            if (Service == "Cln") { return "Clinical"; }
            if (Service == "Del") { return "Delivery"; }
            if (Service == "Gls") { return "Glass"; }
            if (Service == "Gla") { return "Glass"; }
            if (Service == "Box") { return "Box"; }
            if (Service == "Pls") { return "Plastic"; }
            if (Service == "Pla") { return "Plastic"; }
            if (Service == "TRf") { return "TradeRefuse"; }
            if (Service == "TRc") { return "TradeRecycling"; }
            if (Service == "TGr") { return "TradeGarden"; }
            if (Service == "Org") { return "Garden"; }
            if (Service == "Bul") { return "Bulky"; }
            if (Service == "Ser") { return "Service"; }
            if (Service == "Mis") { return "Missed"; }
            if (Service == "AHP") { return "AHP"; }
            throw new InvalidDataException(Service + " is not a valid Service");
        }

        /// <summary>
        /// Set any properties whose types exist in the PropTypeBlackList table to Inactive.
        /// </summary>
        public void ApplyBlacklistToLLPG()
        {
            DataTable TypesToBlacklist = RunQuery(new SqlCommand("SELECT BLPUPropType FROM PropTypeBlackList"), "Collections");

            if (TypesToBlacklist != null)
            {
                foreach (DataRow row in TypesToBlacklist.Rows)
                {
                    SqlCommand ApplyBlackList = new SqlCommand("UPDATE LLPG Set LiveStatus = 'Inactive' WHERE PropType LIKE @Type");
                    ApplyBlackList.Parameters.Add(new SqlParameter("Type", row["BLPUPropType"].ToString()));
                    RunNonQuery(ApplyBlackList, "Collections");
                }
            }
        }

        /// <summary>
        /// Creates a new user in the old Collections Users table
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="council"></param>
        /// <param name="role"></param>
        /// <param name="tableAccess"></param>
        public void AddUser(string email, string password, string council, string role, string tableAccess)
        {
            SqlCommand AddUser = new SqlCommand("IF EXISTS " +
                "(SELECT TOP (1) * FROM Users WHERE Email = @Email) " +
                "UPDATE Users SET Email = @Email, Password = @Password, Council = @Council, Role = @Role, TableAccess = @TableAccess WHERE Email = @Email " +
                "ELSE INSERT INTO Users (Email, Password, Council, Role, TableAccess) VaLUES (@Email, @Password, @Council, @Role, @TableAccess)");
            AddUser.Parameters.Add(new SqlParameter("Email", email));
            AddUser.Parameters.Add(new SqlParameter("Password", password));
            AddUser.Parameters.Add(new SqlParameter("Council", council));
            AddUser.Parameters.Add(new SqlParameter("Role", role));
            AddUser.Parameters.Add(new SqlParameter("TableAccess", tableAccess));

            RunNonQuery(AddUser, "Collections");
        }

        /// <summary>
        /// Lists all roundnames of rounds on a given service
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public List<string> getRoundsForService(string service)
        {
            // Get distinct RoundNames from Collections excluding Admin
            SqlCommand getRoundNames = new SqlCommand("IF EXISTS (SELECT TOP 1 collectsOnASun FROM colClient WHERE collectsOnASun = 'Y') " +
                "SELECT DISTINCT Roundname FROM WaxRoundNew WHERE ServiceName = @Service AND RoundName NOT LIKE 'Admin%' " +
                "ELSE SELECT DISTINCT Roundname FROM WaxRoundNew WHERE ServiceName = @Service AND RoundName NOT LIKE '%Sun%'");
            getRoundNames.Parameters.Add(new SqlParameter("Service", service));

            DataTable WaxRoundNew = RunQuery(getRoundNames, "Collections");
            List<string> roundsForThisService = new List<string>();

            if (WaxRoundNew != null)
                foreach (DataRow row in WaxRoundNew.Rows)
                    roundsForThisService.Add(row["RoundName"].ToString());

            return roundsForThisService;
        }

        /// <summary>
        /// Counts the number of jobs waiting in WMDPerformance
        /// </summary>
        /// <returns></returns>
        public int countWMDPerformanceWaiting()
        {
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");

            string count = RunScalarQuery(new SqlCommand(File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "GetOldWaitingJobs.sql"))), "Collections");
            int int_count = -1;
            Int32.TryParse(count, out int_count);

            if (int_count != -1)
                return int_count;
            else
                return 0;
        }

        /// <summary>
        /// Gets the list of domains the client uses.
        /// e.g. "halton.gov.uk", "dorsetcc.gov.uk"
        /// </summary>
        /// <returns></returns>
        public List<string> GetDomains()
        {
            // Define the query to run
            SqlCommand query = new SqlCommand("SELECT TOP(1) associatedDomains FROM colClient");

            // Get the domains from the database
            // They come hash delimited e.g. "domain1.gov.uk#domain2.gov.uk"
            string hash_delimited_domains = RunScalarQuery(query, "Collections");

            if (hash_delimited_domains != null && hash_delimited_domains.Contains('#'))
                return hash_delimited_domains.Split('#').ToList();
            return new List<string>();
        }
    }
}