﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevOpsConsole
{
    /// <summary>
    /// Records an instance of an erroneous Database value
    /// Contains methods to find, describe and fix that value
    /// </summary>
    public class ErroneousDatum
    {
        private string PrimaryKey;
        private string PrimaryKeyField;
        private string UPRN;
        private string Fieldname;
        private string Table;
        private string Fieldvalue;
        private string reason;

        /// <summary>
        /// Property which can hold a custom SQL Command to correct the erroneous datum.
        /// </summary>
        public SqlCommand correctionStatement { get; set; }

        /// <summary>
        /// Contructs an instance of the ErroneousDatum class with required values
        /// </summary>
        public ErroneousDatum(string PrimaryKey, string PrimaryKeyField, string UPRN, string Fieldname, string Table, string Fieldvalue, string reason)
        {
            this.PrimaryKey = PrimaryKey;
            this.PrimaryKeyField = PrimaryKeyField;
            this.UPRN = UPRN;
            this.Fieldname = Fieldname;
            this.Table = Table;
            this.Fieldvalue = Fieldvalue;
            this.reason = reason;
        }

        /// <summary>
        /// Overrides the ToString() method to describe this value neatly. 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return UPRN + ": " + reason + " (" + Fieldvalue + ")";
        }

        /// <summary>
        /// Returns an SQL Command which locates the erronenous value.
        /// </summary>
        /// <returns></returns>
        public SqlCommand getSQLtoView()
        {
            SqlCommand query = new SqlCommand("SELECT @Fieldname FROM @Table WHERE @PrimaryKeyField = @PrimaryKey");
            query.Parameters.Add(new SqlParameter("Fieldname", Fieldname));
            query.Parameters.Add(new SqlParameter("Table", Table));
            query.Parameters.Add(new SqlParameter("PrimaryKeyField", PrimaryKeyField));
            query.Parameters.Add(new SqlParameter("PrimaryKey", PrimaryKey));
            return query;
        }

        /// <summary>
        /// Returns this object as a comma delimited line of its attributes
        /// </summary>
        /// <returns></returns>
        public string getCSVLine()
        {
            return PrimaryKey + "," +
                PrimaryKeyField + "," +
                UPRN + "," +
                Fieldname + "," +
                Table + "," +
                Fieldvalue + "," +
                reason;
        }
    }
}
