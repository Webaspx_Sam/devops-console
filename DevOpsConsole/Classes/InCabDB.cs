﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DevOpsConsole
{
    /// <summary>
    /// Stores information and methods about a Client.
    /// Inherits from AzureServer which handles the core database querying methods.
    /// </summary>
    public class InCabDB : AzureServer
    {
        #region Attributes and Instantiators
        /// <summary>
        /// The pretty name e.g. "Oadby and Wigston"
        /// </summary>
        public readonly string displayName;
        /// <summary>
        /// The database name e.g. "oadbyandwigston"
        /// </summary>
        public readonly string URLName;

        /// <summary>
        /// The "third format" used by WM Design and the OLD Users tables
        /// e.g. "WestLancs" instead of "westlancs" or "West Lancashire"
        /// </summary>
        public readonly string oldUserTableName;

        // URLName used by AzureMonitoring. To be factored out soon.
        public string Name;
        // Status used by AzureMonitoring
        public string Status;
        // WorkingDays used by AzureMonitoring
        public string WorkingDays;

        /// <summary>
        /// Default constructor. Requires urlname of the client
        /// </summary>
        /// <param name="urlName"></param>
        public InCabDB(string urlName)
        {
            DataTable councilTable = getClientInfo(urlName);

            if (councilTable == null)
                throw new ArgumentException(urlName + " was not a valid council URLName, or it does not have an InCab database.");

            displayName = councilTable.Rows[0]["Name"].ToString();
            URLName = councilTable.Rows[0]["URLname"].ToString();
            oldUserTableName = councilTable.Rows[0]["OldUserTableName"].ToString();
            Status = councilTable.Rows[0]["InCab-Live"].ToString();

            // Inherited from AzureServer
            Databases = new Dictionary<string, string>();
            Databases.Add("collections", councilTable.Rows[0]["CollectionsDBName"].ToString());
            Databases.Add("incab", councilTable.Rows[0]["InCabDBName"].ToString());
            serverString = "Data Source=" + councilTable.Rows[0]["serverString"].ToString() + ".database.windows.net;" +
"Initial Catalog=##Catalog##;" +
"User ID=mike.devine@webaspx.com@" + councilTable.Rows[0]["serverString"].ToString() + ";" +
"password=Web08#580;";

            this.Name = Regex.Replace(displayName, @"\s+", "");
            if (this.Name.ToLower() == "westlancashire")
                this.Name = "WestLancs";

            //this.Status = Status;
            if (Status == "True")
                Status = "Live";
            else
                Status = "NotLive";
        }

        /// <summary>
        /// Retrieve this client's record from the ClientInfo table on WebaspxMaster
        /// </summary>
        /// <param name="urlName"></param>
        /// <returns></returns>
        public DataTable getClientInfo(string urlName)
        {
            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT TOP (1) * FROM ClientInfo JOIN AutomationInfo ON ClientInfo.URLname = AutomationInfo.URLname WHERE ClientInfo.URLname = @urlname AND InCabDBName IS NOT NULL AND InCabDBName != ''", councilGetter);
            getCouncils.Parameters.Add(new SqlParameter("urlname", urlName));
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            return councilTable;
        }
        #endregion

        /// <summary>
        /// Creates a new user in the old InCab Users table
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="council"></param>
        /// <param name="role"></param>
        /// <param name="tableAccess"></param>
        /// <param name="ShowMsg"></param>
        public void AddUser(string email, string password, string council, string role, string tableAccess, bool ShowMsg)
        {
            SqlCommand AddUser = new SqlCommand("IF EXISTS " +
                "(SELECT TOP (1) * FROM Users WHERE Email = @Email) " +
                "UPDATE Users SET Email = @Email, Password = @Password, Council = @Council, Role = @Role, TableAccess = @TableAccess WHERE Email = @Email " +
                "ELSE INSERT INTO Users (Email, Password, Council, Service, Role, TableAccess) VaLUES (@Email, @Password, @Council, 'InCab', @Role, @TableAccess)");
            AddUser.Parameters.Add(new SqlParameter("Email", email));
            AddUser.Parameters.Add(new SqlParameter("Password", password));
            AddUser.Parameters.Add(new SqlParameter("Council", council));
            AddUser.Parameters.Add(new SqlParameter("Role", role));
            AddUser.Parameters.Add(new SqlParameter("TableAccess", tableAccess));

            RunNonQuery(AddUser, "InCab");
        }
    }
}
