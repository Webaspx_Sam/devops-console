﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml.Linq;

namespace DevOpsConsole
{
    public class ScheduledTask
    {
        // this is how the task is identified in the Jobhistory Table and 
        // also its run command in the console
        public string name = "";

        // This is how often the task needs to run (must be in hours)
        // The task scheduler will run a task if it hasn't been run within 75% of its frequency
        // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
        public TimeSpan frequency = TimeSpan.FromHours(24);

        /// <summary>
        /// Comma delimited list of addresses to e-mail the report to
        /// </summary>
        public string recipients;

        /// <summary>
        /// Hook method. Should be overridden by 
        /// </summary>
        public virtual void RunTaskInnerLogic()
        {
            throw new NotImplementedException("No Inherited Method has overridden this placeholder. " +
                "RunTaskInnerLogic only exists in the ScheduledTask class for compilation safety and should be " +
                "overridden by its inheritor.");
        }

        /// <summary>
        /// Standard logic that must be run for each task. Handles timestamping, logging and error catching.
        /// </summary>
        public void RunTaskOuterLogic()
        {
            Console.Title = "DevOps Console - " + name;

            // Record the time the task started
            DateTime start = DateTime.Now;

            // Create a unique task ID for this specific running instance
            // (necessary in case two PCs are running the same task at the same time for some reason)
            // (any ' characters are replaced with the letter 'a' just in case this might cause an SQL error)
            string test = Guid.NewGuid().ToString();

            // Each Guid is 36 characters so we need two to create a random string of length 50
            // (database field is VARCHAR(50))
            string taskID = (Guid.NewGuid().ToString() + Guid.NewGuid()).Replace('\'', 'a').Substring(0, 50);

            // Create start signature using taskID
            RecordStartOfJob("All", name, "Azure Monitoring", "000000", "Job Initialisation", "InProgress", RunTimeData2 : taskID);

            try
            {
                // Run the actual task itself 
                // (this method should be overridden by a task class that inherits from ScheduledTask)
                RunTaskInnerLogic();
                Console.WriteLine("Done");

                // Calculate the time the task took to run and format it as JobHistory requires
                TimeSpan Time_to_Run = DateTime.Now - start;
                string RunTime_HHmmss = Time_to_Run.Hours.ToString("00") + Time_to_Run.Minutes.ToString("00") + Time_to_Run.Seconds.ToString("00");

                // Record the task's success in JobHistry using taskID to identify it
                RecordResultOfJob(taskID, "Success", RunTime_HHmmss);
            }
            catch (Exception Ex)
            {
                // Calculate the time the task took to run (until failure) and format it as JobHistory requires
                TimeSpan Time_to_Run = DateTime.Now - start;
                string RunTime_HHmmss = Time_to_Run.Hours.ToString("00") + Time_to_Run.Minutes.ToString("00") + Time_to_Run.Seconds.ToString("00");

                // Record the fact that the job failed
                RecordResultOfJob(taskID, "Failure", RunTime_HHmmss);

                Console.WriteLine("Error encountered whilst running " + name);
                Console.WriteLine(Ex.Message + "\n\n" + Ex.ToString());

                // Page product owner with details of the Exception so they can fix it
                SendEmail(Ex.Message + "<br><br>" + Ex.ToString(), "DevOps Console Page: Exception Caught during " + name, ReadErrorRecipients());
            }
        }

        /// <summary>
        /// Write a line to the JobHistory table indicating that this task has finished executing, 
        /// and record whether it finished successfully or hit an error.
        /// </summary>
        /// <param name="taskID"></param>
        /// <param name="result"></param>
        /// <param name="runTime"></param>
        public void RecordResultOfJob(string taskID, string result, string runTime)
        {
            AzureServer JobMonitoring = new AzureServer();
            JobMonitoring.serverString = "Data Source=jm201903013.database.windows.net;" +
                "Initial Catalog=##Catalog##;" +
                "User ID=mike.devine@webaspx.com@jm201903013;" +
                "password=Web08#580;";

            SqlCommand updateJob = new SqlCommand("UPDATE JobHistory SET RunSuccessOrFail = @RESULT, RunTime_HHmmss = @RUNTIME WHERE RunTimeData2 = @TASKID;");
            updateJob.Parameters.Add(new SqlParameter("RESULT", result));
            updateJob.Parameters.Add(new SqlParameter("RUNTIME", runTime));
            updateJob.Parameters.Add(new SqlParameter("TASKID", taskID));

            JobMonitoring.RunNonQuery(updateJob, "JobMonitoring", true);
        }

        /// <summary>
        /// Write a line to the JobHistory table indicating that this task has begun execution.
        /// </summary>
        /// <param name="Client"></param>
        /// <param name="ApplicationName"></param>
        /// <param name="TypeOfServer"></param>
        /// <param name="RunTime_HHmmss"></param>
        /// <param name="Description"></param>
        /// <param name="RunSuccessOrFail"></param>
        /// <param name="RunComments"></param>
        /// <param name="RuntimeData1"></param>
        /// <param name="RunTimeData2"></param>
        /// <param name="RunTimeData3"></param>
        public static void RecordStartOfJob(string Client = "", string ApplicationName = "", string TypeOfServer = "",
    string RunTime_HHmmss = "", string Description = "", string RunSuccessOrFail = "", string RunComments = "",
    string RuntimeData1 = "", string RunTimeData2 = "", string RunTimeData3 = "")
        {
            AzureServer JobMonitoring = new AzureServer();
            JobMonitoring.serverString = "Data Source=jm201903013.database.windows.net;" +
                "Initial Catalog=##Catalog##;" +
                "User ID=mike.devine@webaspx.com@jm201903013;" +
                "password=Web08#580;";

            IPHostEntry host; //will need using System.Net
            host = Dns.GetHostEntry(Dns.GetHostName());
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string IPAddress = host.AddressList[1].ToString();

            if (RunTimeData3 == "")
                RunTimeData3 = "Computer Name: " + Environment.MachineName;
            DateTime now = DateTime.Now;
            //string DateTimeRan_yyyyMMdd_HHmm = now.Year.ToString() + now.Month.ToString() + now.Day.ToString() + "_" + now.Hour.ToString() + now.Minute.ToString();
            //string DateTimeRan_yyyyMMdd_HHmm = now.ToString("yyyyMMdd") + "_" + now.Hour.ToString() + now.Minute.ToString();
            string DateTimeRan_yyyyMMdd_HHmm = now.ToString("yyyyMMdd_HHmm");

            SqlCommand SendJobLog = new SqlCommand("INSERT INTO JobHistory " +
                "(Client, IPAddress, ApplicationName, TypeOfServer, DateTimeRanDT, DateTimeRan_yyyyMMdd_HHmm, " +
                "RunTime_HHmmss, Description, RunSuccessOrFail, RunComments, RuntimeData1, RunTimeData2, RunTimeData3) VALUES (" +
                "@Client, @IPAddress, @ApplicationName, @TypeOfServer, @DateTimeRanDT, @DateTimeRan_yyyyMMdd_HHmm, @RunTime_HHmmss, " +
                "@Description, @RunSuccessOrFail, @RunComments, @RunTimeData1, @RunTimeData2, @RunTimeData3);");
            SendJobLog.Parameters.Add(new SqlParameter("Client", Client));
            SendJobLog.Parameters.Add(new SqlParameter("IPAddress", IPAddress));
            SendJobLog.Parameters.Add(new SqlParameter("ApplicationName", ApplicationName));
            SendJobLog.Parameters.Add(new SqlParameter("TypeOfServer", TypeOfServer));
            SendJobLog.Parameters.Add(new SqlParameter("DateTimeRanDT", now));
            SendJobLog.Parameters.Add(new SqlParameter("DateTimeRan_yyyyMMdd_HHmm", DateTimeRan_yyyyMMdd_HHmm)); //DateTime.Now.ToString())); 20191218_1146
            SendJobLog.Parameters.Add(new SqlParameter("RunTime_HHmmss", RunTime_HHmmss));
            SendJobLog.Parameters.Add(new SqlParameter("Description", Description));
            SendJobLog.Parameters.Add(new SqlParameter("RunSuccessOrFail", RunSuccessOrFail));
            SendJobLog.Parameters.Add(new SqlParameter("RunComments", RunComments));
            SendJobLog.Parameters.Add(new SqlParameter("RuntimeData1", RuntimeData1));
            SendJobLog.Parameters.Add(new SqlParameter("RunTimeData2", RunTimeData2));
            SendJobLog.Parameters.Add(new SqlParameter("RunTimeData3", RunTimeData3));

            JobMonitoring.RunNonQuery(SendJobLog, "JobMonitoring", true);
        }

        /// <summary>
        /// Read how often this task is supposed to be ran from the config.xml file.
        /// </summary>
        /// <returns></returns>
        public int ReadFrequency()
        {
            // Acqiure the ApplicationPath using reflection
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");

            // Load the config file using the ApplicationPath to locate the file
            var config = XDocument.Load(Path.Combine(ApplicationPath, "Resources", "Config.xml"));

            // Find the correct element in the xml document then read the frequency value
            var frequency = from task in config.Root.Descendants("Task")
                            where task.Element("name").Value == this.name
                            select task.Element("frequency").Value;

            int.TryParse(frequency.First().ToString(), out int frequencyInHours);
            return frequencyInHours;
        }

        /// <summary>
        /// Retrieves the list of addresses to receive any report this task sends from config.xml
        /// </summary>
        /// <returns></returns>
        public string ReadRecipients()
        {
            // Acqiure the ApplicationPath using reflection
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");

            // Load the config file using the ApplicationPath to locate the file
            var config = XDocument.Load(Path.Combine(ApplicationPath, "Resources", "Config.xml"));

            // Find the correct element in the xml document then read the frequency value
            var recipients = from task in config.Root.Descendants("Task")
                            where task.Element("name").Value == this.name
                            select task.Element("recipients").Value;
            
            return recipients.First().ToString();
        }

        /// <summary>
        /// Retrieves the list of addressse to receive any potential error messages from config.xml
        /// </summary>
        /// <returns></returns>
        public string ReadErrorRecipients()
        {
            try
            {
                var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");

                var config = XDocument.Load(Path.Combine(ApplicationPath, "Resources", "Config.xml"));

                var errorRecipients = from element in config.Root.Descendants("ErrorRecipients")
                                      select element.Value;
                return errorRecipients.First().ToString();
            }
            catch (Exception)
            {
                return "jamie.edwards@webaspx.com";
            }
        }

        /// <summary>
        /// Sends an email from noreply@webaspx.com to a comma delimited string of recipients
        /// Subject is pre-set to Client Website Trawler. Refactor as a parameter if code is re-used.
        /// </summary>
        /// <param name="body">This must be valid HTML and will be the body of the e-mail</param>
        /// <param name="emailList">This must be a single string of comma delimited e-mail addresses</param>
        public void SendEmail(string body, string subject, string emailList)
        {
            // Create an SMTP client with appropriate configuration and credentials 
            using (SmtpClient EmailClient = new SmtpClient() { Port = 587, Host = "smtp.office365.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential("noreply@webaspx.com", "slowSc@rf22") })
            {
                // Create a new Message from noreply addressed to the list of recipients
                using (MailMessage Email = new MailMessage("noreply@webaspx.com", emailList)) // 
                {
                    // Set subject and body 
                    Email.Subject = subject;
                    Email.Body = body;
                    Email.IsBodyHtml = true;

                    // Attempt to send the mail, report error and hang if any exception is encountered
                    Console.WriteLine("Finished. Sending e-mail...");
                    try
                    {
                        EmailClient.Send(Email);
                        Console.WriteLine("E-mail sent successfully.");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Could not send E-mail.\n" + e.ToString());
                        while (true) ;
                    }
                }
            }
        }

        /// <summary>
        /// Compiles a list of current Collection Databases. 
        /// Derives these from Webaspx_Master.ClientInformation.
        /// </summary>
        /// <returns></returns>
        public List<CollectionsDB> getAllColDBs()
        {
            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT ClientInfo.URLname FROM ClientInfo JOIN AutomationInfo ON ClientInfo.URLname = AutomationInfo.URLname WHERE CollectionsDBName IS NOT NULL AND CollectionsDBName != ''", councilGetter);
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            List<CollectionsDB> ColDBs = new List<CollectionsDB>();
            foreach (DataRow row in councilTable.Rows)
                ColDBs.Add(new CollectionsDB(row["URLname"].ToString()));

            return ColDBs;
        }

        /// <summary>
        /// Compiles a list of current InCab Databases. 
        /// Derives these from Webaspx_Master.ClientInformation.
        /// </summary>
        /// <returns></returns>
        public List<InCabDB> getAllInCabDBs()
        {
            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT ClientInfo.URLname FROM ClientInfo JOIN AutomationInfo ON ClientInfo.URLname = AutomationInfo.URLname WHERE InCabDBName IS NOT NULL AND InCabDBName != ''", councilGetter);
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            List<InCabDB> InCabDBs = new List<InCabDB>();
            foreach (DataRow row in councilTable.Rows)
                InCabDBs.Add(new InCabDB(row["URLname"].ToString()));

            return InCabDBs;
        }
    }
}
