﻿using DevOpsConsole.Classes;
using DevOpsConsole.ScheduledTasks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace DevOpsConsole
{
    class DevOpsConsole
    {
        /// <summary>
        /// List of ScheduledTask objects. 
        /// Used to match User Input on key, then run the value.
        /// </summary>
        public Dictionary<string, ScheduledTask> TaskList { get
            {
                Dictionary<string, ScheduledTask> taskList = new Dictionary<string, ScheduledTask>();
                taskList.Add("Apostraphe Remover", new ApostrapheRemover());
                taskList.Add("Backup Deleter", new BackupDeleter());
                taskList.Add("Bulky Job Checker", new BulkyJobChecker());
                taskList.Add("Cloud Optimiser Checker", new CloudOptimiserChecker());
                taskList.Add("Database Fetcher", new DatabaseFetcher());
                taskList.Add("Database Validator", new DBValidator());
                taskList.Add("Login Monitor", new LoginMonitor());
                taskList.Add("Rhondda Depot Manager", new RhonddaDepotManager());
                taskList.Add("Richmondshire Date Extender", new RichmondshireDateExtender());
                taskList.Add("Rounds and Itineraries Matcher", new RoundsItineraryMatcher());
                taskList.Add("WMDPerformance Tidier", new WMDPerformanceTidier());
                taskList.Add("InCab Itineraries Tidier", new InCabItinerariesTidier());
                taskList.Add("Calendar Cacher", new CalendarCacher());


                return taskList;
            } }

        /// <summary>
        /// Second library of independent tasks whichshouldn't be scheduled to run regularly. 
        /// Put one-off routines here, things you want to run at will etc.
        /// </summary>
        public Dictionary<string, Action> AdhocFunctionList {  get
            {
                Dictionary<string, Action> adhocFunctionList = new Dictionary<string, Action>();
                adhocFunctionList.Add("Do all Jobs", DoAllJobs);
                adhocFunctionList.Add("Azure Monitoring", AzureMonitoring);
                adhocFunctionList.Add("Round Investigator", RunRoundInvestigator);

                //adhocFunctionList.Add("Get DPO Status", GetDPOStatus);
                //adhocFunctionList.Add("Run Scheduled Tasks", RunScheduledTasks);

                return adhocFunctionList;
            } }

        /// <summary>
        /// Entry point for the console. Handles human-interaction.
        /// </summary>
        public void RunMainLoop()
        {
            Console.WriteLine("Welcome to the DevOps Console!");

            // Enter the main user interaction loop - only leave it once the user types quit/exit
            string choice = "";
            while (choice.ToLower() != "exit" && choice.ToLower() != "quit")
            {
                Console.Title = "DevOps Console";
                Console.Write("Enter a command or type 'help' for a list of commands:");

                // Receive user-input (block until received)
                choice = Console.ReadLine();

                if (choice.ToLower() == "help")
                    PrintOptions();
                else if (TaskList.ContainsKey(choice))
                    TaskList[choice].RunTaskOuterLogic();
                else if (AdhocFunctionList.ContainsKey(choice))
                    AdhocFunctionList[choice].Invoke();
                else if (choice.ToLower() == "exit" || choice.ToLower() == "quit")
                    break;
                else
                    Console.WriteLine("That is not a recognised function.");
            }
            Console.WriteLine("Goodbye!");
        }

        /// <summary>
        /// List out all available tasks (task name matches the console command to run it)
        /// </summary>
        public void PrintOptions()
        {
            Console.WriteLine("Available functions are as follows:");
            foreach (KeyValuePair<string, ScheduledTask> entry in TaskList)
                Console.WriteLine("\t" + entry.Key);
            foreach (KeyValuePair<string, Action> entry in AdhocFunctionList)
                Console.WriteLine("\t" + entry.Key);
        }

        /// <summary>
        /// Run a task if it is either a Scheduled Task or an Adhoc Function
        /// </summary>
        /// <param name="routineName"></param>
        public void RunRoutine(string routineName)
        {
            // If the argument matches a Key in the dictionary of tasks
            if (TaskList.ContainsKey(routineName))
                TaskList[routineName].RunTaskOuterLogic();
            else if (AdhocFunctionList.ContainsKey(routineName))
                AdhocFunctionList[routineName].Invoke();
            else
                Console.WriteLine("That is not a valid argument for the DevOps Console - execution aborted.");
        }

        /// <summary>
        /// Runs every Scheduled Task one by one in order.
        /// </summary>
        public void DoAllJobs()
        {
            foreach (KeyValuePair<string, ScheduledTask> task in TaskList)
                task.Value.RunTaskOuterLogic();
        }

        /// <summary>
        /// Root report on {success, time, frequency} of all Azure Monitoring routines. 
        /// </summary>
        public void AzureMonitoring()
        {
            Console.Title = "DevOps Console - Azure Monitoring";
            Console.WriteLine("Gathering a report on Azure Monitoring health status...");

            // Create the JobMonitoring table as an instance of AzureServer so it can be queried
            AzureServer JobMonitoring = new AzureServer();
            JobMonitoring.serverString = "Data Source=jm201903013.database.windows.net;" +
                "Initial Catalog=##Catalog##;" +
                "User ID=mike.devine@webaspx.com@jm201903013;" +
                "password=Web08#580;";

            // Use it to construct a table containing each Scheduled Task, when it last ran, its frequency and whether it was successful
            string tableToEmail = BuildMonitoringTable(JobMonitoring, TaskList);

            // E-mail that table to the list of recipients stored in config.xml
            SendEmail(tableToEmail, "Azure Monitoring Overview", ReadAzureMonitoringRecipients());
        }

        /// <summary>
        /// Sends an email from noreply@webaspx.com to a comma delimited string of recipients
        /// </summary>
        /// <param name="body">This must be valid HTML and will be the body of the e-mail</param>
        /// <param name="emailList">This must be a single string of comma delimited e-mail addresses</param>
        public void SendEmail(string body, string subject, string emailList)
        {
            // Create an SMTP client with appropriate configuration and credentials 
            using (SmtpClient EmailClient = new SmtpClient() { Port = 587, Host = "smtp.office365.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential("noreply@webaspx.com", "slowSc@rf22") })
            {
                // Create a new Message from noreply addressed to the list of recipients
                using (MailMessage Email = new MailMessage("noreply@webaspx.com", emailList)) // 
                {
                    // Set subject and body 
                    Email.Subject = subject;
                    Email.Body = body;
                    Email.IsBodyHtml = true;

                    EmailClient.Send(Email);
                }
            }
        }

        /// <summary>
        /// Run the 'Round Investigator' custom routine.
        /// </summary>
        public void RunRoundInvestigator()
        {
            RoundInvestigator roundInvestigator = new RoundInvestigator();
            roundInvestigator.Run();
        }


        /// <summary>
        /// Creates a report on the last run times, results, and frequences of a dictionary of scheduled tasks.
        /// </summary>
        /// <param name="JobMonitoring"></param>
        /// <returns></returns>
        public string BuildMonitoringTable(AzureServer JobMonitoring, Dictionary<string, ScheduledTask> TaskList)
        {
            // Construct the head of the table
            string monitoringTable = "<table border='1' style='width:100%; border: 1; border-color: #f7b391'><tr style='background-color: #1e3663'>" +
                "<th>Routine</th>" +
                "<th>Scheduled Frequency</th>" +
                "<th>Last Run</th>" +
                "<th>Status</th>";

            // Add a row to the table for each Scheduled Task
            foreach (KeyValuePair<string, ScheduledTask> task in TaskList)
            {
                Console.Write("Checking for last run instance of " + task.Key + "...");

                // Pull the latest record of this Task from the JobHistory table
                DataTable JobHistory = JobMonitoring.RunQuery(new SqlCommand("SELECT TOP 1 * FROM JobHistory WHERE ApplicationName = '" + task.Key + "' ORDER BY ID DESC;"), "JobMonitoring", true);

                // If there is no record then the job must never have been ran - add a table row reflecting that information
                if (JobHistory == null)
                {
                    monitoringTable += "<tr><td>" + task.Key + "</td><td>" + task.Value.frequency.TotalHours.ToString() + " hours</td>" + "<td>" + @"<p style='background-color: #ff0000; color: white'><b>Never</b></p></td><td>NA</td></tr>";
                    Console.WriteLine("Never");
                }
                else
                {
                    DateTime lastRecord = (DateTime)JobHistory.Rows[0]["DateTimeRanDT"];
                    string status = JobHistory.Rows[0]["RunSuccessOrFail"].ToString();
                    if (status == "Failure")
                        status = "<p style='background-color: #ff0000; color: white'><b>" + status + "</b></p>";

                    // Convert the DateTime the task was last run into a human-readable format
                    // such as "10 hours ago" or "3 days ago" 
                    TimeSpan TimeSinceLastRun = DateTime.Now - lastRecord;
                    string prettyTimeSince = LastRunInPrettyForm(lastRecord);
                    Console.WriteLine(prettyTimeSince);

                    monitoringTable += "<tr><td>" + task.Key + "</td><td>" + task.Value.frequency.TotalHours.ToString() + " hours</td>" + "<td>";

                    // If the task has run within its frequency period
                    if (task.Value.frequency > TimeSinceLastRun)

                        // And if the task has run within half of its frequency period
                        if (task.Value.frequency.Divide(2) > TimeSinceLastRun)
                            // Add a normal row to the table with no extra bells or whistles
                            monitoringTable += @"<p><b>" + prettyTimeSince + " </b></p></td>";
                        else // the task has ran within its frequency but not within half its frequency, so add a row coloured orange as a warning
                            monitoringTable += @"<p style = 'background-color: #ef6724; color: white'><b>" + prettyTimeSince + " </b></p></td>";
                    else // the task has ran outside its frequency, add a row coloured in red
                        monitoringTable += @"<p style = 'background-color: #ff0000; color: white'><b>" + prettyTimeSince + " </b></p></td>";

                    monitoringTable += "<td>" + status + "</td></tr>";
                }
            }

            // Close the table and return the built html as a string
            return monitoringTable + "</table>";
        }

        /// <summary>
        /// Read a list of addresses to receive the overall 'checklist of tasks' report from config.xml
        /// </summary>
        /// <returns></returns>
        public string ReadAzureMonitoringRecipients()
        {
            try
            {
                var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
                //string recipients = File.ReadAllText(Path.Combine(ApplicationPath, "Resources", "ReportRecipients.txt"));
                //return recipients;

                var config = XDocument.Load(Path.Combine(ApplicationPath, "Resources", "Config.xml"));

                var reportRecipients = from element in config.Root.Descendants("ReportRecipients")
                                       select element.Value;
                return reportRecipients.First().ToString();
            }
            catch (Exception)
            {
                return "jamie.edwards@webaspx.com";
            }
        }

        /// <summary>
        /// Convert the DateTime the task was last run into a human-readable format
        /// </summary>
        /// <param name="DateLoggedIn"></param>
        /// <returns></returns>
        public string LastRunInPrettyForm(DateTime DateLoggedIn)
        {
            int difference = (DateTime.Now - DateLoggedIn).Days;
            if (difference == 0)
            {
                double hourDif = Math.Round((DateTime.Now - DateLoggedIn).TotalHours, 2);
                if (hourDif < 1.0)
                    return "Just now";
                else if (hourDif >= 1.0 && hourDif < 2.0)
                    return "1 hour ago";
                else
                    return hourDif.ToString() + " hours ago";
            }
            //return "Today";
            if (difference == 1)
            {
                double hourDif = Math.Round((DateTime.Now - DateLoggedIn).TotalHours, 2);
                return "Yesterday (" + hourDif.ToString() + " hours ago)";
            }

            if (difference > 1 && difference < 31)
                return difference.ToString() + " days ago";
            if (difference > 30)
                return "Over a month ago";
            return DateLoggedIn.ToString("yyyy/MM/dd HH:mm:ss");
        }

        /// <summary>
        /// Compiles a list of current Collection Databases. 
        /// Derives these from Webaspx_Master.ClientInformation.
        /// </summary>
        /// <returns></returns>
        public List<CollectionsDB> getAllColDBs()
        {
            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT ClientInfo.URLname FROM ClientInfo JOIN AutomationInfo ON ClientInfo.URLname = AutomationInfo.URLname WHERE CollectionsDBName IS NOT NULL AND CollectionsDBName != ''", councilGetter);
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            List<CollectionsDB> ColDBs = new List<CollectionsDB>();
            foreach (DataRow row in councilTable.Rows)
                ColDBs.Add(new CollectionsDB(row["URLname"].ToString()));

            return ColDBs;
        }

        /// <summary>
        /// Compiles a list of current InCab Databases. 
        /// Derives these from Webaspx_Master.ClientInformation.
        /// </summary>
        /// <returns></returns>
        public List<InCabDB> getAllInCabDBs()
        {
            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT ClientInfo.URLname FROM ClientInfo JOIN AutomationInfo ON ClientInfo.URLname = AutomationInfo.URLname WHERE InCabDBName IS NOT NULL AND InCabDBName != ''", councilGetter);
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            List<InCabDB> InCabDBs = new List<InCabDB>();
            foreach (DataRow row in councilTable.Rows)
                InCabDBs.Add(new InCabDB(row["URLname"].ToString()));

            return InCabDBs;
        }
    }
}
