﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace DevOpsConsole.Classes
{
    public class RoundInvestigator
    {
        string URLName;
        int crew;
        string day;
        int week;
        string service;
        string recipients;
        InCabDB incab;
        CollectionsDB collections;

        string Description { get { return "Cr" + crew + " " + day + " Wk" + week + " " + service; } }
        /// <summary>
        /// Entry Point
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Welcome to the round investigator. Please answer these questions or enter 'quit' or 'exit' at any time.");
            try { GatherDetails(); } catch (QuitEarlyException)
            {
                Console.WriteLine("\tAborting...");
                return;
            }

            try { Investigate(); } 
            catch ( SqlException Ex)
            {
                // blah blah
            }

            return;
        }

        public void GatherDetails()
        {
            URLName = PromptForInput("Please enter the URLName of the client in question: (i.e. 'ardsandnorthdown' NOT 'ards and north down");
            int.TryParse(PromptForInput("Please enter the crew number: (just the number)"), out crew);
            day = PromptForInput("Please enter the day in Collections Format: (i.e. 'Mon', 'Thu')");
            int.TryParse(PromptForInput("Please enter the week number: (just the number)"), out crew);
            service = PromptForInput("Please enter the service in Collections Format: (i.e. 'Ref', 'Grn')");
            recipients = PromptForInput("Please enter a comma delimited list of e-mails to report to:");
        }

        public string PromptForInput(string prompt = "Enter a value: ")
        {
            while (true)
            {
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                if (input.ToLower() == "quit" || input.ToLower() == "exit")
                    throw new QuitEarlyException();
                else if (!string.IsNullOrWhiteSpace(input))
                    return input;
            }
        }

        public void Investigate()
        {
            Console.WriteLine("\nInvestigating " + Description + " for " + URLName + "...");
            try { incab = new InCabDB(URLName); } catch (Exception) { Console.WriteLine("\tNo incab database found for " + URLName); }
            try { collections = new CollectionsDB(URLName); } catch (Exception) { Console.WriteLine("\tNo collections database found for " + URLName); }

            Console.WriteLine("\nInvestigating " + collections.displayName + " " + Description + "...");

            PrintWhenLastOptimised();
            DataTable incabItinerary = ContentsInIncabItinerary();
            
            Console.WriteLine("");
            DataTable WaxRoundNewContents = ContentsInCollections();
            DataTable incabSchedule = ContentsInIncabSchedule();

            Console.WriteLine("");
            PrintWhenLastLoggedIn();

            DumpQueries();

            /////////////////////////////////////////
            EmailLatestItinerary(incabItinerary);
        }

        public void EmailLatestItinerary(DataTable itinerary)
        {
            using (SmtpClient EmailClient = new SmtpClient() { Port = 587, Host = "smtp.office365.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential("noreply@webaspx.com", "slowSc@rf22") })
            {
                using (MailMessage Email = new MailMessage("noreply@webaspx.com", recipients)) // 
                {
                    // Add the zipped itinerary
                    if (itinerary != null)
                    {
                        byte[] itineraryBytes = (byte[])itinerary.Rows[0]["itineraryTag"];
                        string filename = "Itinerary_Cr" + crew + "_" + day + "_Wk" + week + "_" + service + ".zip";
                        Email.Attachments.Add(new Attachment(new MemoryStream(itineraryBytes), filename, MediaTypeNames.Application.Zip));
                    }

                    Email.Subject = this.Description;
                    Email.Body = "<p>Please find attached: Latest itinerary for " + this.Description + ".</p>";
                    Email.IsBodyHtml = true;

                    EmailClient.Send(Email);
                }
            }
        }

        #region Stuff
        public void PrintWhenLastOptimised()
        {
            SqlCommand FindLastOptimised = new SqlCommand("SELECT TOP (1) * FROM WMDPerformance " +
                "WHERE Day = @day " +
                "AND Week = @week " +
                "AND ServiceName = @service " +
                "AND RoundName LIKE '%' + @crew + '%' " +
                "ORDER BY ID DESC");
            FindLastOptimised.Parameters.Add(new SqlParameter("crew", crew.ToString()));
            FindLastOptimised.Parameters.Add(new SqlParameter("service", service));
            FindLastOptimised.Parameters.Add(new SqlParameter("day", day));
            FindLastOptimised.Parameters.Add(new SqlParameter("week", week.ToString()));

            DataTable WMDPerformance = collections.RunQuery(FindLastOptimised, "Collections");

            if (WMDPerformance == null)
            {
                Console.WriteLine("\tNo entry found in WMDPerformance for " + Description + ". This implies it had never been optimised by Cloud Optimiser.");
                return;
            }

            try
            {
                string timeOptimised = WMDPerformance.Rows[0]["Results"].ToString().Split('#')[0];

                Console.WriteLine("\tLast optimised: " + timeOptimised + " with status " + WMDPerformance.Rows[0]["Status"].ToString());
            } catch (Exception)
            {
                Console.WriteLine("\t Error encountered whilst calculating time last optimised.");
            }
        }

        public void PrintWhenLastLoggedIn()
        {
            SqlCommand FindLastLogin = new SqlCommand("SELECT TOP (1) * FROM InCabLogIns " +
                "WHERE Crew = @crew " +
                "AND DayDDD = @day " +
                "AND WeekNo = @week " +
                "AND Serv3L = @service " +
                "ORDER BY ID DESC;");
            FindLastLogin.Parameters.Add(new SqlParameter("crew", crew.ToString()));
            FindLastLogin.Parameters.Add(new SqlParameter("service", service));
            FindLastLogin.Parameters.Add(new SqlParameter("day", day));
            FindLastLogin.Parameters.Add(new SqlParameter("week", week.ToString()));

            DataTable result = incab.RunQuery(FindLastLogin, "incab");

            if (result == null)
            {
                Console.WriteLine("\tCannot find any record of a truck logging into this round in InCabLogIns.");
                return;
            } else
            {
                Console.WriteLine("\tLast truck to log into this round was:");
                Console.WriteLine("\t\t" + result.Rows[0]["Reg"].ToString());
                string decimalTime = result.Rows[0]["DigTime"].ToString();
                float.TryParse(decimalTime, out float deciTime);

                string TimeToPrint = TimeSpan.FromHours(deciTime).ToString("h\\:mm");
                Console.WriteLine("\t\tat " + TimeToPrint);
                Console.WriteLine("\t\ton " + DateTime.ParseExact(result.Rows[0]["DateYYYYMMDD"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy/MM/dd"));
                Console.WriteLine("\t\twith Driver ID: " + result.Rows[0]["DriverID"].ToString());
                Console.WriteLine("\t\trunning InCab Software Version: " + result.Rows[0]["SWVers"].ToString());
            }
        }

        public DataTable ContentsInIncabItinerary()
        {
            SqlCommand GetItineraryContents = new SqlCommand("SELECT TOP (1) * FROM IncabItinerary " +
                "WHERE itineraryRound = @crew " +
                "AND itineraryService = @service " +
                "AND itineraryDay = @day " +
                "AND itineraryWeek = @week " +
                "ORDER BY itineraryID DESC;");
            GetItineraryContents.Parameters.Add(new SqlParameter("crew", crew.ToString()));
            GetItineraryContents.Parameters.Add(new SqlParameter("service", ServiceToRoundshopFormat(service)));
            GetItineraryContents.Parameters.Add(new SqlParameter("day", day));
            GetItineraryContents.Parameters.Add(new SqlParameter("week", week.ToString()));

            DataTable result = incab.RunQuery(GetItineraryContents, "incab");
            if (result == null)
            {
                Console.WriteLine("\tNo entries found in InCabItinerary for " + Description + ".");
                return new DataTable();
            } else
            {
                Console.WriteLine("\tLast Itinerary produced: " + result.Rows[0]["itineraryDate"].ToString() + " " + result.Rows[0]["itineraryTime"].ToString() + ".");
                return result;
            }
        }

        public DataTable ContentsInIncabSchedule()
        {
            SqlCommand GetScheduleContents = new SqlCommand("SELECT * FROM InCabSchedule " +
                "WHERE Service = @service " +
                "AND Day = @day " +
                "AND Week LIKE '%' + @week " +
                "AND Crew = @crew; ");
            GetScheduleContents.Parameters.Add(new SqlParameter("crew", crew.ToString()));
            GetScheduleContents.Parameters.Add(new SqlParameter("service", ServiceToRoundshopFormat(service)));
            GetScheduleContents.Parameters.Add(new SqlParameter("day", day));
            GetScheduleContents.Parameters.Add(new SqlParameter("week", week.ToString()));

            DataTable result = incab.RunQuery(GetScheduleContents, "incab");

            if (result == null)
            {
                Console.WriteLine("\tNo entries found in InCabSchedule for " + Description + ".");
                return new DataTable();
            } else
            {
                Console.WriteLine("\t" + result.Rows.Count + " properties on this schedule in InCabSchedule.");
                return result;
            }
        }

        public DataTable ContentsInCollections()
        {
            SqlCommand GetCollectionsContents = new SqlCommand("SELECT LLPG.UPRN, WaxRoundNew.Crew, " +
                "WaxRoundNew.Day_DDD, WaxRoundNew.Week, WaxRoundNew.ServiceName, Assisted, Assisted_ind FROM LLPG " +
                "JOIN LLPGXTra ON LLPG.UPRN = LLPGXtra.UPRN " +
                "JOIN WaxRoundNew ON LLPG.UPRN = WaxRoundNew.UPRN " +
                "WHERE WaxRoundNew.ServiceName = @service " +
                "AND Week = 'Wk' + @week " +
                "AND Day_DDD = @day " +
                "AND RoundName LIKE '%Cr' + @crew + '%';");
            GetCollectionsContents.Parameters.Add(new SqlParameter("crew", crew.ToString()));
            GetCollectionsContents.Parameters.Add(new SqlParameter("service", service));
            GetCollectionsContents.Parameters.Add(new SqlParameter("day", day));
            GetCollectionsContents.Parameters.Add(new SqlParameter("week", week.ToString()));

            DataTable result = collections.RunQuery(GetCollectionsContents, "Collections");

            if (result == null)
            {
                Console.WriteLine("\tNothing found in WaxRoundNew for this round.");
                return new DataTable();
            } else
            {
                Console.WriteLine("\t" + result.Rows.Count + " properties on this round in WaxRoundNew.");
                return result;
            }
        }

        /// <summary>
        /// Prints out the queries used but swaps out the parameterised variables with the values actually used.
        /// </summary>
        public void DumpQueries()
        {
            Console.WriteLine("\nQueries used:");

            Console.WriteLine("\nTo find out when last optimised:");
            Console.WriteLine("\tSELECT TOP (1) * FROM WMDPerformance ");
            Console.WriteLine("\tWHERE Day = '" + day + "' ");
            Console.WriteLine("\tAND Week = '" + week + "' ");
            Console.WriteLine("\tAND ServiceName = '" + service + "' ");
            Console.WriteLine("\tAND RoundName LIKE '%' + '" + crew + "' + '%' ");
            Console.WriteLine("\tORDER BY ID DESC");

            Console.WriteLine("\nTo find properties in WaxRoundNew on this round:");
            Console.WriteLine("\tSELECT LLPG.UPRN, WaxRoundNew.Crew, WaxRoundNew.Day_DDD, ");
            Console.WriteLine("\tWaxRoundNew.Week, WaxRoundNew.ServiceName, Assisted, Assisted_ind FROM LLPG ");
            Console.WriteLine("\tJOIN LLPGXTra ON LLPG.UPRN = LLPGXtra.UPRN ");
            Console.WriteLine("\tJOIN WaxRoundNew ON LLPG.UPRN = WaxRoundNew.UPRN ");
            Console.WriteLine("\tWHERE WaxRoundNew.ServiceName = '" + service + "' ");
            Console.WriteLine("\tAND Week = 'Wk' + '" + week + "' ");
            Console.WriteLine("\tAND Day_DDD = '" + day + "' ");
            Console.WriteLine("\tAND RoundName LIKE '%Cr' + '" + crew + "' + '%';");

            Console.WriteLine("\nTo find the latest itinerary to download:");
            Console.WriteLine("\tSELECT TOP (1) * FROM IncabItinerary ");
            Console.WriteLine("\tWHERE itineraryRound = '" + crew + "' ");
            Console.WriteLine("\tAND itineraryService = '" + ServiceToRoundshopFormat(service) + "' ");
            Console.WriteLine("\tAND itineraryDay = '" + day + "' ");
            Console.WriteLine("\tAND itineraryWeek = '" + week + "' ");
            Console.WriteLine("\tORDER BY itineraryID DESC;");

            Console.WriteLine("\nTo find properties in IncabSchedule on this round:");
            Console.WriteLine("\tSELECT * FROM InCabSchedule ");
            Console.WriteLine("\tWHERE Service = '" + ServiceToRoundshopFormat(service) + "' ");
            Console.WriteLine("\tAND Day = '" + day + "' ");
            Console.WriteLine("\tAND Week LIKE '%' + '" + week + "' ");
            Console.WriteLine("\tAND Crew = '" + crew + "'; ");

            Console.WriteLine("\nTo find the last truck login from InCabLogins:");
            Console.WriteLine("\tSELECT TOP (1) * FROM InCabLogIns ");
            Console.WriteLine("\tWHERE Crew = '" + crew + "' ");
            Console.WriteLine("\tAND DayDDD = '" + day + "' ");
            Console.WriteLine("\tAND WeekNo = '" + week + "' ");
            Console.WriteLine("\tAND Serv3L = '" + service + "' ");
            Console.WriteLine("\tORDER BY ID DESC");
        }
        #endregion

        public string ServiceToRoundshopFormat(string Service)
        {
            if (Service == "Ref") { return "Refuse"; }
            if (Service == "Rec") { return "Recycling"; }
            if (Service == "Grn") { return "Garden"; }
            if (Service == "Tra") { return "Trade"; }
            if (Service == "Trd") { return "Trade"; }
            if (Service == "Foo") { return "Food"; }
            if (Service == "Cln") { return "Clinical"; }
            if (Service == "Del") { return "Delivery"; }
            if (Service == "Gls") { return "Glass"; }
            if (Service == "Gla") { return "Glass"; }
            if (Service == "Box") { return "Box"; }
            if (Service == "Pls") { return "Plastic"; }
            if (Service == "Pla") { return "Plastic"; }
            if (Service == "TRf") { return "TradeRefuse"; }
            if (Service == "TRc") { return "TradeRecycling"; }
            if (Service == "TGr") { return "TradeGarden"; }
            if (Service == "Org") { return "Garden"; }
            if (Service == "Bul") { return "Bulky"; }
            if (Service == "Ser") { return "Service"; }
            if (Service == "Mis") { return "Missed"; }
            if (Service == "AHP") { return "AHP"; }
            throw new InvalidDataException(Service + " is not a valid Service");
        }
    }

    public class QuitEarlyException : Exception
    {
        public QuitEarlyException()
        {
        }

        public QuitEarlyException(string message)
            : base(message)
        {
        }

        public QuitEarlyException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
