﻿using DevOpsConsole.Classes;
using DevOpsConsole.ScheduledTasks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Linq;

namespace DevOpsConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "DevOps Console"; 
            DevOpsConsole DevOpsConsole = new DevOpsConsole();

            // If there are command line arguments they should be names of routines, so try to run them
            if (args.Count() > 0)
                foreach (string arg in args)
                    DevOpsConsole.RunRoutine(arg);
            
            // Else open up the console for manual user input
            else
                DevOpsConsole.RunMainLoop();
        }
    }
}
