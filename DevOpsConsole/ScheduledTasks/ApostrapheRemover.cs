﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class ApostrapheRemover : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public ApostrapheRemover()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Apostraphe Remover";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            //frequency = TimeSpan.FromHours(24);
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Beginning Apostraphe removal...");

            List<CollectionsDB> CollectionsClients = getAllColDBs().Where(p => p.Status == "Live").ToList();

            foreach (CollectionsDB colDB in CollectionsClients)
            {
                Console.WriteLine("Removing Apostraphes from " + colDB.displayName + " LLPG:");
                RemoveApostraphes(colDB, "LLPG");

                Console.WriteLine("Removing Apostraphes from " + colDB.displayName + " LLPGXtra:");
                RemoveApostraphes(colDB, "LLPGXTra");
            }
        }


        /// <summary>
        /// Removes all occurances of "&apos&comma" from a given table and replaces each with "&apos;"
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="colDB"></param>
        public void RemoveApostraphes(CollectionsDB colDB, string tableName)
        {
            // Load any rows from the table which contain "apos"
            // This requires the sp_FindStringInTable stored procedure on all Collections databases. 
            //          it can be found at Resources/FindStringInTable.sql, just run it in
            DataTable tableToClean = colDB.RunQuery(new SqlCommand("EXEC sp_FindStringInTable '%&apos&comma%', 'dbo', '" + tableName + "'"), "Collections");

            if (tableToClean == null)
            {
                Console.WriteLine("\tNo occurances of the '&apos&comma' string were found in " + tableName);
                return;
            }
            // Loop through each cell of the table
            foreach (DataColumn col in tableToClean.Columns)
            {
                foreach (DataRow row in tableToClean.Rows)
                {
                    // If any cell contains "&apos&comma"
                    if (row[col].ToString().Contains("&apos&comma"))
                    {
                        Console.Write("Found '&apos&comma' in the " + col.ColumnName + " column: ");

                        // Create the new string with the replaced value
                        string replacedString = row[col].ToString().Replace("&apos&comma", "&apos;");
                        Console.WriteLine("Replacing " + row[col].ToString() + " with " + replacedString);

                        SqlCommand updateCommand = new SqlCommand(String.Format("UPDATE [{0}] SET [{1}] = @ReplacedString WHERE [UPRN] = @UPRN", tableName, col.ColumnName));
                        updateCommand.Parameters.Add(new SqlParameter("ReplacedString", replacedString));
                        updateCommand.Parameters.Add(new SqlParameter("UPRN", row["UPRN"].ToString()));

                        SqlCommand historyCommand = new SqlCommand(String.Format("INSERT INTO [History] ([UPRN], [ChangeType], [WhatChanged], [ChangeDate], [ChangeUser])" +
                                                        " VALUES (@UPRN,'{0} Update aposcomma', @ReplacedString, GetDate(), 'WebaspxDataValidator')", tableName));
                        historyCommand.Parameters.Add(new SqlParameter("ReplacedString", replacedString));
                        historyCommand.Parameters.Add(new SqlParameter("UPRN", row["UPRN"].ToString()));

                        // Replace the value in the DB with the new value
                        colDB.RunNonQuery(updateCommand, "Collections");
                        // Record what changes in the history table
                        colDB.RunNonQuery(historyCommand, "Collections");
                    }
                }
            }
        }
    }
}
