﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DevOpsConsole.ScheduledTasks
{
    class BackupDeleter : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public BackupDeleter()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Backup Deleter";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Deleting Backups...");
            List<string> BackupInfo = DeleteBackups();

            string output = "";
            if (BackupInfo.Count > 0)
            {
                output += "<h3>Backup Info:</h3><pre>";
                foreach (string value in BackupInfo)
                {
                    output += value + "\n";
                }
                output += "</pre>";
            }

            if (output != "")
            {
                SendEmail(output, "Backup Deleter", recipients);
            }
        }

        private static List<string> DeleteBackups()
        {
            List<string> listDeletorInfo = new List<string>();

            List<FileInfo> FirstOfTheMonth = null;

            List<string> directories = new List<string>();
            try
            {
                directories = Directory.GetDirectories(@"W:\").Where(x => !x.Contains('@')).ToList();
            } catch (DirectoryNotFoundException Ex)
            {
                throw new Exception("Could not connect to the W drive.\n\n" + Ex.Message);
            }
            

            foreach (string Client in directories)
            {
                Console.WriteLine("Cleaning " + Client);
                //eg Client at this point contains W:\\ArdsAndNorthDown
                System.Threading.Thread.Sleep(1000);

                Double MBSaved = 0;

                foreach (String Service in Directory.GetDirectories(Client))
                {
                    //Service is either Collections InCab or Workflow
                    //Service here contains eg W:\\ArdsAndNorthDown\\Collections
                    //or                       W:\\ArdsAndNorthDown\\incab
                    Console.WriteLine("Cleaning " + Client + " " + Service);
                    Boolean InCab = Service.Contains("incab");

                    Boolean Collections = Service.Contains("Collections");

                    if (Collections)
                    {
                        FirstOfTheMonth = GetFirst(Service);
                    }

                    FileInfo[] BackupFiles = new DirectoryInfo(Service).GetFiles();
                    //Backupfiles is an array / FileInfo Class of all the back up files in this directory
                    foreach (FileInfo Backup in BackupFiles)
                    {
                        //eg BackupFiles[0].Name    would contain   2019-07-24--17_Facilities.csv
                        //Backup.FullName           would contain   W:\ArdsAndNorthDown\incab\2019-07-24--17_Facilities.csv
                        //Backup.DirectoryName      would contain   W:\ArdsAndNorthDown\incab
                        //Backup.Directory.Name     would contain   incab
                        String RecyclingName = Backup.FullName.Replace(Client, String.Format("W:\\@Recycle\\{0}", Client.Replace("W:\\", ""))); //Path.Combine(Backup.FullName.Replace(Backup.Name, ""), "@Recycle", Backup.Name);
                        //RecyclingName             would contain   W:\@Recycle\ArdsAndNorthDown\incab\2019-07-24--17_Facilities.csv
                        if (InCab)
                        {
                            String[] Last3Backups = BackupFiles.OrderByDescending(x => x.CreationTime).Select(x => x.Name.Substring(0, 10)).ToList().Distinct().Take(3).ToArray();
                            //Last3Backups  - array of the Dates of the last 3 backups
                            //eg
                            //  Last3Backups[0]=2019-07-30
                            //  Last3Backups[1]=2019-07-25
                            //  Last3Backups[2]=2019-07-24
                            String[] Last30Backups = BackupFiles.OrderByDescending(x => x.CreationTime).Select(x => x.Name.Substring(0, 10)).ToList().Distinct().Take(30).ToArray();
                            Int32 DaysAgo = ((Int32)((DateTime.Today - File.GetCreationTime(Backup.FullName).Date).TotalDays));
                            Boolean IsThisFileTheFirstBackupCSVOfTheCurrentAsYoureLoopingThroughTheLoopMonth = FirstOfTheMonth.Count(x => x.Name == Backup.Name) < 1;
                            if (Backup.Name.Contains("_Users.csv") || Backup.Name.Contains("_inCabIssues.csv"))
                            {
                                if ((DaysAgo > 30) && IsThisFileTheFirstBackupCSVOfTheCurrentAsYoureLoopingThroughTheLoopMonth)
                                {
                                    //if users file or inCabIssues, keep last 30 days plus first of the month
                                    try
                                    {
                                        File.Delete(Backup.FullName);

                                        File.Delete(RecyclingName);

                                        MBSaved += (Backup.Length / 1024f) / 1024f;
                                    }
                                    catch
                                    { }
                                }
                            }
                            else
                            {
                                if (!Last3Backups.Contains(Backup.Name.Substring(0, 10)))
                                {
                                    try
                                    {
                                        File.Delete(Backup.FullName);

                                        File.Delete(RecyclingName);

                                        MBSaved += (Backup.Length / 1024f) / 1024f;
                                    }
                                    catch { }
                                }
                            }
                        }
                        else if (Collections)
                        {
                            Int32 DaysAgo = ((Int32)((DateTime.Today - File.GetCreationTime(Backup.FullName).Date).TotalDays));

                            Boolean IsThisFileTheFirstBackupCSVOfTheCurrentAsYoureLoopingThroughTheLoopMonth = FirstOfTheMonth.Count(x => x.Name == Backup.Name) < 1;

                            if ((DaysAgo > 30) && IsThisFileTheFirstBackupCSVOfTheCurrentAsYoureLoopingThroughTheLoopMonth)
                            {
                                try
                                {
                                    File.Delete(Backup.FullName);

                                    File.Delete(RecyclingName);

                                    MBSaved += (Backup.Length / 1024f) / 1024f;
                                }
                                catch
                                { }
                            }
                        }
                    }
                }

                if (MBSaved > 0.0)
                    listDeletorInfo.Add(Regex.Replace(Client, "W:", "") + ": deleted " + Math.Round(MBSaved, 2).ToString() + "mb.");
                else
                    listDeletorInfo.Add(Regex.Replace(Client, "W:", "") + ": " + "No backups deleted.");
            }
            return listDeletorInfo;
        }

        private static List<FileInfo> GetFirst(String Service)
        {
            List<FileInfo> ToKeep = new List<FileInfo>();

            Dictionary<String, List<FileInfo>> Data = new Dictionary<String, List<FileInfo>>();

            foreach (FileInfo file in new DirectoryInfo(Service).GetFiles())
            {
                String Month = String.Format("{0}-{1}", file.CreationTime.Month, file.CreationTime.Year);

                if (Data.ContainsKey(Month))
                {
                    Data[Month].Add(file);
                }
                else
                {
                    Data.Add(Month, new List<FileInfo>() { file });
                }
            }

            foreach (var file in Data)
            {
                ToKeep.AddRange(file.Value.Where(x => x.CreationTime.Date == file.Value.OrderBy(y => y.CreationTime).FirstOrDefault().CreationTime.Date).ToList());
            }

            return ToKeep;
        }
    }
}
