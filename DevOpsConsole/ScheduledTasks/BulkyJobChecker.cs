﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class BulkyJobChecker : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public BulkyJobChecker()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Bulky Job Checker";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Checking for clients (just Copeland right now) with 4 or more Waiting Cloud Optimiser Jobs...");

            List<string> warningList = GetJobListWaiting();
            if (warningList.Count > 0)
            {
                Console.WriteLine("Found " + warningList.Count.ToString() + " waiting jobs:");

                string emailBody = "<h3>Cloud Optimiser Jobs Waiting</h3><pre>";
                foreach (string job in warningList)
                {
                    emailBody += job + "\n";
                    Console.WriteLine(job);
                }
                emailBody += "</pre>";
                Console.WriteLine("Sending e-mail...");
                SendEmail(emailBody, "Copeland Bulky Jobs Waiting", recipients);
            }
            else
            {
                Console.WriteLine("No waiting jobs found");
            }
        }

        private List<string> GetJobListWaiting()
        {
            List<CollectionsDB> CollectionsClients = getAllColDBs().Where(p => p.Status == "Live").ToList();

            List<string> waitingJobs = new List<string>();

            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
            string QueryString = File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "GetWaitingOptimiserJobs.sql"));

            SqlCommand FindWaitingCopelandJobs = new SqlCommand(QueryString);
            FindWaitingCopelandJobs.CommandTimeout = 90;

            foreach (CollectionsDB colDB in CollectionsClients)
            {
                if (colDB.URLName == "copeland" || colDB.URLName == "york" || colDB.URLName == "falkirk")
                {
                    DataTable WaitingJobs = colDB.RunQuery(FindWaitingCopelandJobs, "Collections");
                    if (WaitingJobs != null)
                    {
                        foreach (DataRow row in WaitingJobs.Rows)
                        {
                            if (row["JobStatus"].ToString().ToLower() == "waiting")
                                waitingJobs.Add("Client: " + colDB.displayName +
                                    " JobID: " + row["JobID"].ToString() +
                                    " JobType: " + row["JobType"].ToString() +
                                    " JobCreationDate: " + row["CreationDate"].ToString());
                        }
                    }
                }
            }

            return waitingJobs;
        }
    }
}
