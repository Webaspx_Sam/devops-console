﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using Webaspx.Database;

namespace DevOpsConsole.ScheduledTasks
{
    class CalendarCacher : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public CalendarCacher()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Calendar Cacher";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Beginning to Cache Collections Calendars...");

            List<CollectionsDB> CollectionsClients = getAllColDBs().Where(p => p.Status == "Live").ToList();
            foreach (CollectionsDB colDB in CollectionsClients)
            {
                CreateCachedCalendar(colDB);
            }
        }

        /// <summary>
        /// Truncates then re-populates the CachedCalendars table.
        /// This marries every UPRN to a uniqueReference informing on its round configuration.
        /// It is used for producing calendar printoffs and to give South Norfolk a minimal required dataset 
        /// to derive some info they want without calling the main method once per property.
        /// </summary>
        private static void CreateCachedCalendar(CollectionsDB colDB)
        {
            Console.WriteLine("Getting unique Rounds with UPRNs (" + colDB.displayName + ")");

            // Enumerate each UPRN (second column) and each round that UPRN belongs to (columns 3, 4, 5.... etc.)
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
            string QueryString = File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "GetRoundsperUPRN.sql"));

            DataTable roundsPerUPRN = colDB.RunQuery(new SqlCommand(QueryString), "Collections");

            // Convert the table of UPRN with rounds into a minimal data set:
            // Each unique set of rounds is given an index and each UPRN belongs to an index
            // This produces a smaller table to search through - need only search for a UPRN and return its Round index
            List<string[]> UPRN_RoundsRows = CreateRoundIndex(roundsPerUPRN, colDB);

            Console.WriteLine("Writing CachedCalendars Messages (" + colDB.displayName + ")");

            // CommandsToRun is a list of SQL commands that need to be executed
            List<string> CommandsToRun = new List<string>();

            // First command will be to truncate the CachedCalendars table
            CommandsToRun.Add("Truncate Table CachedCalendars");

            // Add an INSERT command for each UPRN that has active rounds
            foreach (string[] RoundArray in UPRN_RoundsRows)
            {
                if (RoundArray[1] != "") //only write if rounds present
                {
                    CommandsToRun.Add("INSERT INTO CachedCalendars (UPRN, rounds, uniqueReference) VALUES " +
                        "('" + RoundArray[0] + "', '" + RoundArray[1] + "', '" + RoundArray[2] + "');");
                }
            }

            // Run in each command and report progress every 100 commands
            int noOfRows = 0; // count number of commands executed
            foreach (string commandString in CommandsToRun)
            {
                noOfRows++;
                colDB.RunNonQuery(new SqlCommand(commandString), "Collections");
                if (noOfRows % 100 == 0)
                {
                    string comp = noOfRows + "/" + UPRN_RoundsRows.Count; // eg "100/500"
                    Console.WriteLine("Writing CachedCalendars (" + colDB.displayName + ")" + comp);
                }
            }
        }

        /// <summary>
        /// Converts a table of UPRNs with rounds into a minimal data set:
        /// Each unique set of rounds is given an index and each UPRN belongs to an index
        /// This produces a smaller table to search through - need only search for a UPRN and return its Round index
        /// </summary>
        /// <param name="roundsPerUPRN"></param>
        /// <param name="colDB"></param>
        /// <returns></returns>
        private static List<string[]> CreateRoundIndex(DataTable roundsPerUPRN, CollectionsDB colDB)
        {
            // Create a list of LIVE roundnames in the RoundShop
            // Conditions for a round being LIVE vary according to whether CollectsOnASunday is true
            SqlCommand getRoundNames = new SqlCommand("IF EXISTS (SELECT TOP 1 collectsOnASun FROM colClient WHERE collectsOnASun = 'Y') " +
                "SELECT roundname FROM roundshop WHERE roundname NOT LIKE 'Admin%' ORDER BY displayorder + 0 ELSE " +
                "SELECT roundname FROM roundshop WHERE roundname NOT LIKE '%Sun Wk%' ORDER BY displayorder + 0");
            DataTable RawroundNames = colDB.RunQuery(getRoundNames, "Collections");

            // Transform the 'roundname' column of RawroundNames into a list
            List<string> roundNames = new List<string>();
            foreach (DataRow row2 in RawroundNames.Rows)
            {
                roundNames.Add(row2["roundname"].ToString());
            }

            // UPRN_RoundsRows will be a list of arrays such that:
            // [0] is UPRN, [1] is a $ delimited list of rounds, [2] is a integer uniquereference that calendars lookup
            // This is added to by each loop iteration and returned at the end
            List<string[]> UPRN_RoundsRows = new List<string[]>();

            // This is a list of unique round combinations
            // The indicies of this list are used as the uniquereference for each UPRN / RoundCombo pair
            List<string> uniqueCombinations = new List<string>();

            foreach (DataRow UPRNRow in roundsPerUPRN.Rows)
            {
                // This is an array representing one row of the larger UPRN_RoundsRows list, so:
                // [0] is UPRN, [1] is a $ delimited list of rounds, [2] is a integer uniquereference that calendars lookup
                string[] uprnRoundsRows = new string[3];

                // Add the UPRN to the first index of uprnRoundsRows
                uprnRoundsRows[0] = UPRNRow[1].ToString();

                // Add each value of the Datatable that contains a roundname to uprnRoundsRows[1], delimited by a $
                foreach (var columnValue in UPRNRow.ItemArray)
                {
                    if (roundNames.Contains(columnValue))
                        uprnRoundsRows[1] += columnValue.ToString() + "$";
                }

                // If the combination doesn't yet exist in uniqueCombinations, add it then use its index as the uniquereference
                // Else: add the combination's index as the unique reference  
                if (uprnRoundsRows[1] != null)
                {
                    if (!uniqueCombinations.Contains(uprnRoundsRows[1]))
                    {
                        uniqueCombinations.Add(uprnRoundsRows[1]);
                        uprnRoundsRows[2] = uniqueCombinations.IndexOf(uprnRoundsRows[1]).ToString();
                    }
                    else
                    {
                        uprnRoundsRows[2] = uniqueCombinations.IndexOf(uprnRoundsRows[1]).ToString();
                    }

                    UPRN_RoundsRows.Add(uprnRoundsRows);
                }
            }
            return UPRN_RoundsRows;
        }
    }
}
