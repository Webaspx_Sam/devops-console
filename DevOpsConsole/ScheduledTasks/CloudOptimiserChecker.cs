﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class CloudOptimiserChecker : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public CloudOptimiserChecker()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Cloud Optimiser Checker";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Checking for clients with 4 or more Waiting Cloud Optimiser Jobs...");
            DateTime start = DateTime.Now;

            List<string> warningList = GetCloudOptimiserWaiting();
            if (warningList.Count > 0)
            {
                Console.WriteLine("Found " + warningList.Count.ToString() + " waiting jobs:");

                string emailBody = "<h3>Cloud Optimiser Jobs Waiting</h3><pre>";
                foreach (string job in warningList)
                {
                    emailBody += job + "\n";
                    Console.WriteLine(job);
                }
                emailBody += "</pre>";

                SendEmail(emailBody, "Cloud Optimiser Jobs Waiting", recipients);
            }

            //TimeSpan Time_to_Run = DateTime.Now - start;
            //string RunTime_HHmmss = Time_to_Run.Hours.ToString("00") + Time_to_Run.Minutes.ToString("00") + Time_to_Run.Seconds.ToString("00");
            //SendJobLog("All", "Cloud Optimiser Job Checker", "Azure Monitoring", RunTime_HHmmss, "Job Completion", "Success");
        }

        private List<string> GetCloudOptimiserWaiting()
        {
            List<InCabDB> InCabClients = getAllInCabDBs().Where(p => p.Status == "Live").ToList();
            List<CollectionsDB> CollectionsClients = getAllColDBs().Where(p => p.Status == "Live").ToList();

            // Derive a list of Collections Databases for which there are matching InCab Databases   (Essentially doing an INNER JOIN on both lists over the URLName attribute)
            List<CollectionsDB> ClientsWithBoth = CollectionsClients.Where(x => InCabClients.Any(y => x.URLName == y.URLName)).ToList();

            // Prepare an empty list to fill with warnings found
            List<string> warningList = new List<string>();

            // Fill warninglist with a message for each Database which has more than 2 jobs currently waiting
            foreach (CollectionsDB server in ClientsWithBoth)
            {
                Console.Write("Checking " + server.displayName + "... ");
                int countWaiting = server.countWMDPerformanceWaiting();
                Console.WriteLine(countWaiting.ToString() + " found.");
                if (countWaiting > 3)
                    warningList.Add(server.Name + " " + countWaiting + " *** OPS TEAM - CHECK CLOUD OPTIMISER INSTANCE FOR " + server.Name + " ***");
            }

            return warningList;
        }
    }
}
