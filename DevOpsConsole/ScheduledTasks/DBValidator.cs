﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DevOpsConsole.ScheduledTasks
{
    public class DBValidator : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public DBValidator()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Database Validator";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            DeletePreviousReports();

            Console.WriteLine("Beginning Validation Checks on Collections Database...");
            string CollectionsErrorTable = produceCollectionsErrorTable();

            Console.WriteLine("Beginning Validation Checks on InCab Database...");
            string InCabErrorTable = produceInCabErrorTable();

            Console.WriteLine("Finished Validation Checks, sending e-mail...");
            SendEmail(CollectionsErrorTable + InCabErrorTable, "Database Validation", recipients);
        }

        /// <summary>
        /// Validates all Collections databases and returns any errors found in a table. 
        /// Table contains Client, Type of Error, Location of full Report. 
        /// </summary>
        /// <returns></returns>
        public string produceCollectionsErrorTable()
        {
            // Gather a list of all Collections and InCab Databases. 
            List<CollectionsDB> allCollectionsDBs = getAllColDBs();

            // Open the table and create the header 
            string errorTable = "";
            errorTable += "<table border='1' style='width:100%; border: 1; border-color: #f7b391'><tr style='background-color: #1e3663'>" +
                "<th>Council</th>" +
                "<th>Type of Error</th>" +
                "<th>No of occurances</th>" +
                "<th>Location of Report</th>" +
                "</tr>";

            // Ask each Collections database to check itself
            foreach (CollectionsDB colDB in allCollectionsDBs)
            {
                if (colDB.URLName != "dwp")
                {
                    // This writes reports on any erroneous data to T:\Azure Monitoring\Automated 2019\Reports
                    List<List<string>> thisDBerrors = performAllChecks(colDB);

                    // Create a table row for each error found in this Database
                    foreach (List<String> error in thisDBerrors)
                    {
                        errorTable += "<tr><td>" + error[0] + "</td><td>" + error[1] + "</td><td>" + error[2] + "</td><td>" + error[3] + "</td></tr>";
                    }
                }
            }

            // Close off the table
            errorTable += "</table>";

            // Return the complete table
            return errorTable;
        }

        /// <summary>
        /// Validates all Collections databases and returns any errors found in a table. 
        /// Table contains Client, Type of Error, Location of full Report. 
        /// </summary>
        /// <returns></returns>
        public string produceInCabErrorTable()
        {
            // Gather a list of all Collections and InCab Databases. 
            List<InCabDB> allInCabDBs = getAllInCabDBs();

            // Open the table and create the header 
            string errorTable = "";
            errorTable += "<table border='1' style='width:100%; border: 1; border-color: #f7b391'><tr style='background-color: #1e3663'>" +
                "<th>Council</th>" +
                "<th>Type of Error</th>" +
                "<th>No of occurances</th>" +
                "<th>Location of Report</th>" +
                "</tr>";

            // Ask each InCab database to check itself
            foreach (InCabDB InCabDB in allInCabDBs)
            {
                List<List<string>> thisDBerrors = performAllChecks(InCabDB);
                // Create a table row for each error found in this Database
                foreach (List<String> error in thisDBerrors)
                {
                    errorTable += "<tr><td>" + error[0] + "</td><td>" + error[1] + "</td><td>" + error[2] + "</td><td>" + error[3] + "</td></tr>";
                }
            }

            // Close off the table
            errorTable += "</table>";

            // Return the complete table
            return errorTable;
        }

        /// <summary>
        /// Converts a list of ErroneousDatums into a comma delimited string. 
        /// Output can be written straight into a .csv file.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public string createErrorReport(List<ErroneousDatum> errors)
        {
            var csv = new StringBuilder();
            // Add the headers:
            csv.AppendLine("PrimaryKey,PrimaryKeyField,UPRN, Fieldname,Table,Fieldvalue,reason");

            // For each erroneous datum add a comma delimited line of its values
            foreach (ErroneousDatum error in errors)
            {
                csv.AppendLine(error.getCSVLine());
            }

            // Return the csv-ready string
            return csv.ToString();
        }

        /// <summary>
        /// Splits Roundname up into their Components in order
        ///  [0] : Crew [1] :  Day [2] : Week [3] : Service
        /// </summary>
        /// <param name="Roundname"></param>
        /// <returns></returns>
        public List<string> getRoundnameComponents(string Roundname)
        {
            // Split the Crew/Day/Week/Service out of the Roundname
            List<String> Components = Roundname.Split(' ').ToList();

            // Return only those components, ignoring anything that came before
            return Components.GetRange(Components.Count - 4, 4);
        }

        #region Validation Functions
        /// <summary>
        /// Returns whethers a string matches the Collections Roundname format. 
        /// </summary>
        /// <param name="Roundname"></param>
        /// <returns></returns>
        public bool validateRoundname(string Roundname)
        {
            // Returns false immediatey if Roundname is null
            if (Roundname == null)
                return false;

            // Returns false immediately if Roundname is too short to possibly contain a valid string
            if (Roundname.Length < 14)
                return false;

            // Returns false if Roundname contains values that Collections delimits on
            if (Roundname.Contains("#") || Roundname.Contains("$") || Roundname.Contains(","))
                return false;

            /// Defines the Regular Expression for:
            /// {anything}Cr{numeral}{0 or 1 numerals}{space}{3 letters}{space}Wk{numeral}{space}{3 letters}{end}
            /// To test if the roundname is in format: Anything Cr1-99 Mon Wk1-9 Ref
            Match match = Regex.Match(Roundname, @".*Cr[1-9A-Z][0-9]?\s[A-Za-z]{3}\sWk[a-z0-9][a-z0-9]?\s[A-Za-z]{3}$");

            // Returns false if Roundname doesn't match that regular expression
            if (!match.Success)
                return false;

            // Try to split the Roundname up into its components
            // Hitting an exception means either there wasn't a ' ' to split on or 
            // that there weren't enough items in the resulting list (out of range)
            // Theoretically neither is possible because the Regex Match above would already return false
            List<string> RoundComponents = new List<String>();
            try { RoundComponents = getRoundnameComponents(Roundname); }
            catch (Exception) { return false; }

            // Validate each individual component
            if (!validateCollectionsCrew(RoundComponents[0]))
                return false;

            if (!validateDay(RoundComponents[1]))
                return false;

            if (!validateWeek(RoundComponents[2]))
                return false;

            if (!validateCollectionsService(RoundComponents[3]))
                return false;

            // If we haven't returned false yet then Roundname has passed all the checks
            return true;
        }


        public bool validateDay(string Day)
        {
            // Fail immediately if the input was null
            if (Day == null)
                return false;

            /// Enumerate the valid options for the Day Component
            /// Return false if Day isn't one of these
            List<String> LegalDays = new List<String>{
                "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
            };
            if (!LegalDays.Contains(Day))
                return false;
            return true;
        }

        public bool validateWeek(string Week)
        {
            // Fail immediately if the input was null
            if (Week == null)
                return false;

            // Week can be "Wk1-53" or "Wkly", this handles the Wkly case
            if (Week == "Wkly")
                return true;

            // Week can be as high as 53 but not 54
            // Return true if Week matches a Regular Expression for Wk1-49 or is the other few cases
            if (Week == "Wk50" || Week == "Wk51" || Week == "Wk52" || Week == "Wk53")
                return true;
            Match match = Regex.Match(Week, @"^Wk[1-4][0-9]?$");

            return (match.Success);
        }

        /// <summary>
        /// Evaluates whether a string is a valid Collection Service name
        /// </summary>
        /// <param name="Service"></param>
        /// <returns></returns>
        public bool validateCollectionsService(string Service)
        {
            // Fail immediately if the input was null
            if (Service == null)
                return false;

            /// Enumerate the valid options for the Service Component
            /// Return false if Service isn't one of these
            List<String> LegalServices = new List<String>{
                "Ref", "Rec", "Grn", "AHP", "Gls", "Org", "TRf", "Tra", "TGr",
                "Box", "AHP", "Pls", "Del", "Foo", "Bul", "Mis", "Ser", "Cln",
                "TRc"
            };
            if (!LegalServices.Contains(Service))
                return false;
            return true;
        }

        /// <summary>
        /// Evaluates whether a string is a valid InCab Service name
        /// </summary>
        /// <param name="Service"></param>
        /// <returns></returns>
        public bool validateInCabService(string Service)
        {
            // Fail immediately if the input was null
            if (Service == null)
                return false;

            /// Enumerate the valid options for the Service Component
            /// Return false if Service isn't one of these
            List<String> LegalServices = new List<String>{
                "refuse", "recycling", "trade", "glass", "plastic", "organic", "food",
                "garden", "clinical", "delivery", "box", "traderefuse", "traderecycling", "tradegarden",
                "refusebags", "servicebins", "missedbins", "bulky", "service", "ahp", "blackbox"
            };
            if (!LegalServices.Contains(Service.ToLower()))
                return false;
            return true;
        }

        /// <summary>
        /// Evaluates whether a string is a valid Collections Crew
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public bool validateCollectionsCrew(string Crew)
        {
            // Fail immediately if the input was null
            if (Crew == null)
                return false;

            // Define a regular expression for "Cr{number from 1-9}{number from 0-9}
            // So Cr1 is fine, Cr11 is fine, but Cr01 is not fine. 
            // Neither is Cr0 nor Cr100
            Match numericMatch = Regex.Match(Crew, @"^Cr[1-9][0-9]?$");
            Match alphabetMatch = Regex.Match(Crew, @"^Cr[A-Z]$");

            // Return whether Crew matches that expression
            if (!numericMatch.Success && !alphabetMatch.Success)
                return false;
            return true;
        }

        /// <summary>
        /// Evaluates whether a string is a valid InCab Crew
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public bool validateInCabCrew(string Crew)
        {
            // Fail immediately if the input was null
            if (Crew == null)
                return false;

            int Prospective_Crew = -1;
            Match match = Regex.Match(Crew, @"^[A-Z]$");
            if (!match.Success && !Int32.TryParse(Crew, out Prospective_Crew))
                return false;

            // Fail validation if Crew either failed or parsed OR parsed but was not 0-99 inclusive. 
            if (!match.Success && (Prospective_Crew < 0 || Prospective_Crew > 99))
                return false;

            // Pass otherwise
            return true;
        }

        /// <summary>
        /// Evaluates whether a string is a valid InCab Week
        /// </summary>
        /// <param name="Week"></param>
        /// <returns></returns>
        public bool validateInCabWeek(string Week)
        {
            // Fail immediately if the input was null
            if (Week == null)
                return false;

            // Define the regular expression of an InCab Week
            Match match = Regex.Match(Week.ToLower(), @"^week\s?[1-9][0-9]?$");

            // Returns false if Week doesn't match that regular expression
            if (!match.Success)
                return false;

            // Strip the number out of week. We know from the Regex check that by now we MUST contain 'week' 
            // so split on the 'k' of week then trim any whitespace that may or not prepend the number
            string Week_stripped = Week.ToLower().Split('k')[1].Trim();

            // Try to parse the stripped values into an integer
            int Prospective_WeekNo = -1;
            if (!Int32.TryParse(Week_stripped, out Prospective_WeekNo))
                return false;

            // Now check the extracted Week Number is 0-53 inclusive
            if (Prospective_WeekNo < 0 || Prospective_WeekNo > 53)
                return false;

            // Pass otherwise
            return true;
        }

        /// <summary>
        /// Evaluates whether a string is a valid UPRN
        /// </summary>
        /// <param name="UPRN"></param>
        /// <returns></returns>
        public bool validateUPRN(string UPRN)
        {
            // UPRN fails validation if it is null
            if (UPRN == null)
                return false;

            // UPRN fails validation if it is too short to be a valid UPRN
            if (UPRN.Length < 6)
                return false;

            // Try to parse the UPRN as an integer
            long Prospective_UPRN = 0;
            if (!Int64.TryParse(UPRN, out Prospective_UPRN))
                return false;

            // If EITHER: The UPRN failed to parse, the UPRN parsed but was 0, or the UPRN parsed but was negative
            // ... fail validation
            if (Prospective_UPRN < 1)
                return false;

            // Return true if it's passed all the checks
            return true;
        }

        /// <summary>
        /// Compares a Day/Week/Service triplet against the Roundname
        /// Returns whether the triplet matches the information in Roundname
        /// </summary>
        /// <param name="Day"></param>
        /// <param name="Week"></param>
        /// <param name="Service"></param>
        /// <param name="Roundname"></param>
        /// <returns></returns>
        public bool validateComponents(string Day, string Week, string Service, string Roundname)
        {
            // Split Roundname into its components
            List<string> Components = getRoundnameComponents(Roundname);

            // Compare each variable against its relevant component
            // Return false if any don't match
            if (!(Day.ToLower() == Components[1].ToLower()))
                return false;

            if (!(Week.ToLower() == Components[2].ToLower()))
                return false;

            if (!(Service.ToLower() == Components[3].ToLower()))
                return false;

            // Return true if they've all matched
            return true;
        }
#endregion

        #region Collections Validation Methods
        /// <summary>
        /// Performs each and every data validation check.
        /// Writes reports on any erroneous data to the Shared Drive. 
        /// </summary>
        public List<List<string>> performAllChecks(CollectionsDB colDB)
        {
            List<List<string>> ErrorsFound = new List<List<string>>();

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Roundnames for " + colDB.displayName);
            List<ErroneousDatum> erroneousRoundnames = checkRoundnames(colDB);
            if (erroneousRoundnames.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousRoundnames.Count.ToString());
                // Create a unique filepath derived from Clientname and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_InvalidRoundnames_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousRoundnames));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Invalid Rounds", erroneousRoundnames.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Days and write a report on any found
            Console.WriteLine("Checking Day_DDD for " + colDB.displayName);
            List<ErroneousDatum> erroneousDays = checkDay_DDD(colDB);
            if (erroneousDays.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousDays.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_InvalidDays_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousDays));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Invalid Days", erroneousDays.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Weeks and write a report on any found
            Console.WriteLine("Checking Week (col) for " + colDB.displayName);
            List<ErroneousDatum> erroneousWeeks = checkWeek(colDB);
            if (erroneousWeeks.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousWeeks.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_InvalidWeeks_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousWeeks));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Invalid Weeks", erroneousWeeks.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Services and write a report on any found
            Console.WriteLine("Checking ServiceName for " + colDB.displayName);
            List<ErroneousDatum> erroneousServices = checkService(colDB);
            if (erroneousServices.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousServices.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_InvalidServices_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousServices));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Invalid Services", erroneousServices.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Day/Week/Service triplets and write a report on any found
            Console.WriteLine("Checking Day_DDD/Week/Service Triplets for " + colDB.displayName);
            List<ErroneousDatum> erroneousTriplets = checkRoundComponentsMatch(colDB);
            if (erroneousTriplets.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousTriplets.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_InvalidDayWeekServices_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousTriplets));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Day/Week/Service Triplets Don't match RoundName", erroneousTriplets.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for duplicate Day/Week/Service triplets and write a report on any found
            Console.WriteLine("Checking for Duplicate Day_DDD/Week/Service Triplets for " + colDB.displayName);
            List<ErroneousDatum> duplicateTriplets = checkDuplicateRoundComponents(colDB);
            if (duplicateTriplets.Count > 0)
            {
                Console.WriteLine("\tFound: " + duplicateTriplets.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_DuplicateRoundComponents_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(duplicateTriplets));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Duplicate Day/Week/Service Triplets for same UPRN", duplicateTriplets.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for duplicate roundnames and write a report on any found
            Console.WriteLine("Checking For Duplicate Roundnames for " + colDB.displayName);
            List<ErroneousDatum> duplicateRoundnames = checkDuplicateRoundnames(colDB);
            if (duplicateRoundnames.Count > 0)
            {
                Console.WriteLine("\tFound: " + duplicateRoundnames.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_DuplicateRoundnames_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(duplicateRoundnames));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Duplicate Roundnames for same UPRN", duplicateRoundnames.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for roundnames that do not appear in the RoundShop
            Console.WriteLine("Checking for Rounds not in RoundShop for " + colDB.displayName);
            List<ErroneousDatum> unsupportedRoundnames = checkRoundsInRoundShop(colDB);
            if (unsupportedRoundnames.Count > 0)
            {
                Console.WriteLine("\tFound: " + unsupportedRoundnames.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_MissingRoundnames_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(unsupportedRoundnames));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Roundnames not in RoundShop", unsupportedRoundnames.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for bins that do not appear in the Binshop
            Console.WriteLine("Checking Bins not in BinShop for " + colDB.displayName);
            List<ErroneousDatum> unsupportedBins = checkBinsInBinShop(colDB);
            if (unsupportedBins.Count > 0)
            {
                Console.WriteLine("\tFound: " + unsupportedBins.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_MissingBins_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(unsupportedBins));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Bins not in BinShop", unsupportedBins.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for Assisted or SingleSided that are not Yes or No
            Console.WriteLine("Checking Assisted and SingleSided are either Yes or No for " + colDB.displayName);
            List<ErroneousDatum> Assisinglesideds = checkAssistedAndSingleSided(colDB);
            if (Assisinglesideds.Count > 0)
            {
                Console.WriteLine("\tFound: " + Assisinglesideds.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_NotBinaryAssistedOrSingleSided_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(Assisinglesideds));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Assisted/SingleSided not Yes/No", Assisinglesideds.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check failed Cloud Optimiser Jobs
            Console.WriteLine("Checking Failed Cloud Optimiser Jobs for " + colDB.displayName);
            List<ErroneousDatum> failedJobs = checkFailedOptimiserJobs(colDB);
            if (failedJobs.Count > 0)
            {
                Console.WriteLine("\tFound: " + unsupportedBins.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    colDB.URLName + "_FailedCloudOptimiserJobs_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(failedJobs));
                ErrorsFound.Add(new List<string> { colDB.displayName, "Failed Cloud Optimiser Jobs", failedJobs.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            return ErrorsFound;
        }

        /// <summary>
        /// Validates each Roundname in WaxRoundNew.
        /// </summary>
        /// <returns>A list of Primary Keys of Roundnames which failed validation.</returns>
        public List<ErroneousDatum> checkRoundnames(CollectionsDB colDB)
        {
            // Get all Rounds from WaxRoundNew along with their primary key
            DataTable WaxRoundNew = colDB.RunQuery(new SqlCommand("SELECT CollectionID, UPRN, RoundName FROM WaxRoundNew"), "Collections");

            // Validate each roundname and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    if (!validateRoundname(row["RoundName"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "RoundName",
                            "WaxRoundNew",
                            row["RoundName"].ToString(),
                            "Roundname failed to validate."));
                }
            }

            // Return the primary keys of any rounds which failed validation
            return erroneousDatums;
        }

        public List<ErroneousDatum> checkAssistedAndSingleSided(CollectionsDB colDB)
        {
            DataTable BadStatusFields = colDB.RunQuery(new SqlCommand("SELECT CollectionID, LLPG.UPRN, Assisted, SingleSided FROM WaxRoundNew JOIN LLPG ON LLPG.UPRN = WaxRoundNew.UPRN " +
                "WHERE Assisted NOT IN ('Yes', 'No') OR SingleSided NOT IN ('Yes', 'No')"), "Collections");

            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (BadStatusFields != null)
            {
                foreach (DataRow row in BadStatusFields.Rows)
                {
                    erroneousDatums.Add(new ErroneousDatum(
                        row["CollectionID"].ToString(),
                        "CollectionID",
                        row["UPRN"].ToString(),
                        "Assisted or SingleSided",
                        "WaxRoundNew or LLPG",
                        "Assisted: " + row["Assisted"].ToString() + " SingleSided: " + row["SingleSided"].ToString(),
                        "Assisted or SingleSided was not Yes or No."));
                }
            }

            // Return the primary keys of any rounds which failed validation
            return erroneousDatums;
        }

        public List<ErroneousDatum> checkFailedOptimiserJobs(CollectionsDB colDB)
        {
            DataTable distinctOptimisedRounds = colDB.RunQuery(new SqlCommand("SELECT DISTINCT RoundName FROM WMDPerformance"), "Collections");

            List<string> failedMarkers = new List<string>()
            {
                "failed", "no data", "download"
            };

            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (distinctOptimisedRounds == null)
                return erroneousDatums;

            foreach (DataRow round in distinctOptimisedRounds.Rows)
            {
                DataTable latestRunFailed = colDB.RunQuery(new SqlCommand("SELECT TOP 1 * FROM WMDPerformance WHERE RoundName = '" + round["RoundName"].ToString() + "'"), "Collections");
                if (latestRunFailed != null)
                {
                    if (failedMarkers.Contains(latestRunFailed.Rows[0]["Status"].ToString().ToLower()))
                    {
                        erroneousDatums.Add(new ErroneousDatum(
                            latestRunFailed.Rows[0]["ID"].ToString(),
                            "ID",
                            "NA",
                            "Status",
                            "No Data, Failed or Download",
                            latestRunFailed.Rows[0]["Status"].ToString(),
                            "Latest optimised itinerary for this round failed."));
                    }
                }
            }

            // Return the primary keys of any rounds which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Day_DDD in WaxRoundNew.
        /// </summary>
        /// <returns>A list of Primary Keys of Days which failed validation.</returns>
        public List<ErroneousDatum> checkDay_DDD(CollectionsDB colDB)
        {
            // Get all Rounds from WaxRoundNew along with their primary key
            DataTable WaxRoundNew = colDB.RunQuery(new SqlCommand("SELECT CollectionID, UPRN, Day_DDD FROM WaxRoundNew"), "Collections");

            // Validate each Day and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    if (!validateDay(row["Day_DDD"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "Day_DDD",
                            "WaxRoundNew",
                            row["Day_DDD"].ToString(),
                            "Day failed to validate."));
                }
            }

            // Return the primary keys of any rounds which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Week in WaxRoundNew.
        /// </summary>
        /// <returns>A list of Primary Keys of Weeks which failed validation.</returns>
        public List<ErroneousDatum> checkWeek(CollectionsDB colDB)
        {
            // Get all Rounds from WaxRoundNew along with their primary key
            DataTable WaxRoundNew = colDB.RunQuery(new SqlCommand("SELECT CollectionID, UPRN, Week FROM WaxRoundNew"), "Collections");

            // Validate each Week and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    if (!validateWeek(row["Week"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "Week",
                            "WaxRoundNew",
                            row["Week"].ToString(),
                            "Week failed to validate."));
                }
            }


            // Return the primary keys of any rounds which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Service in WaxRoundNew.
        /// </summary>
        /// <returns>A list of Primary Keys of Services which failed validation.</returns>
        public List<ErroneousDatum> checkService(CollectionsDB colDB)
        {
            // Get all Rounds from WaxRoundNew along with their primary key
            DataTable WaxRoundNew = colDB.RunQuery(new SqlCommand("SELECT CollectionID, UPRN, ServiceName FROM WaxRoundNew"), "Collections");

            // Validate each roundname and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    if (!validateCollectionsService(row["ServiceName"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "ServiceName",
                            "WaxRoundNew",
                            row["ServiceName"].ToString(),
                            "Service failed to validate."));
                }
            }


            // Return the primary keys of any Services which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Checks that the Day/Week/Service values for all UPRNs match their
        /// corresponding values in Roundname.
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkRoundComponentsMatch(CollectionsDB colDB)
        {
            // Get all Roundnames and Round Components from WaxRoundNew
            DataTable WaxRoundNew = colDB.RunQuery(new SqlCommand("SELECT CollectionID, UPRN, Day_DDD, Week, ServiceName, RoundName FROM WaxRoundNew"), "Collections");

            // Validate each triplet against the RoundName and store the Primary Keys of any that fail
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    if (!validateComponents(row["Day_DDD"].ToString(), row["Week"].ToString(), row["ServiceName"].ToString(), row["RoundName"].ToString()))
                    {
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "ServiceName",
                            "WaxRoundNew",
                            row["Day_DDD"].ToString() + "#" + row["Week"].ToString() + "#" + row["ServiceName"].ToString() + "#" + row["RoundName"].ToString(),
                            "Day Week or Service failed to match values in Roundname."));
                    }
                }
            }


            // Return the primary keys of any Triplets which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Checks that no UPRNs exist twice on the same Roundname.
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkDuplicateRoundnames(CollectionsDB colDB)
        {
            // Parse the stored Query
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
            string QueryString = File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "UPRNsWithDuplicateRoundnames.sql"));

            SqlCommand FindDuplicateUPRNs = new SqlCommand(QueryString);
            FindDuplicateUPRNs.CommandTimeout = 90;

            // Get all Roundnames from WaxRoundNew
            DataTable WaxRoundNew = colDB.RunQuery(FindDuplicateUPRNs, "Collections");

            // Record errors for any rows where a UPRN/Roundname pairs exists more than once
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    int occurances = -1;
                    Int32.TryParse(row["row_num"].ToString(), out occurances);
                    if (occurances > 1)
                    {
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "RoundName",
                            "WaxRoundNew",
                            row["RoundName"].ToString(),
                            "This is the second occurances of this Roundname for this UPRN"));
                    }
                }
            }

            // Return the primary keys of any erroneous rows
            return erroneousDatums;
        }

        /// <summary>
        /// Checks that no UPRNs have two entries with identical Day/Week/Service components.
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkDuplicateRoundComponents(CollectionsDB colDB)
        {
            // Parse the stored Query
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
            SqlCommand FindDuplicateComponents = new SqlCommand(File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "UPRNsWithDuplicateRoundComponents.sql")));
            FindDuplicateComponents.CommandTimeout = 90;

            // Get all Round Components from WaxRoundNew
            DataTable WaxRoundNew = colDB.RunQuery(FindDuplicateComponents, "Collections");

            // Record errors for any rows where a UPRN/RoundComponentTriplet pairs exists more than once
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    int occurances = -1;
                    Int32.TryParse(row["row_num"].ToString(), out occurances);
                    if (occurances > 1)
                    {
                        erroneousDatums.Add(new ErroneousDatum(
                            row["CollectionID"].ToString(),
                            "CollectionID",
                            row["UPRN"].ToString(),
                            "Day Week Service and Crew",
                            "WaxRoundNew",
                            row["Day_DDD"].ToString() + "#" + row["Week"].ToString() + "#" + row["ServiceName"].ToString() + row["Crew"].ToString(),
                            "This is the second occurances of this Day/Week/Service quadruplet for this UPRN"));
                    }
                }
            }

            // Return the primary keys of any erroneous rows
            return erroneousDatums;
        }

        /// <summary>
        /// Checks that all containers in the Bins table exist in the BinShop table.
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkBinsInBinShop(CollectionsDB colDB)
        {
            // Parse the stored Query
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
            SqlCommand FindBins = new SqlCommand(File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "BinsNotInShop.sql")));
            FindBins.CommandTimeout = 90;


            // Get all Round Components from WaxRoundNew
            DataTable Bins = colDB.RunQuery(FindBins, "Collections");

            // Record errors for any rows where a bin does not exist in the bin shop
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Bins != null)
            {
                foreach (DataRow row in Bins.Rows)
                {
                    erroneousDatums.Add(new ErroneousDatum(
                        "Multiple",
                        "ID",
                        "Multiple",
                        "Bin_number",
                        "Bins",
                        row["Bin_number"].ToString(),
                        "This bin does not exist in the Bin Shop."));
                }
            }

            // Return the list of errors
            return erroneousDatums;
        }

        /// <summary>
        /// Checks that all round names in WaxRoundNew exist in the RoundShop.
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkRoundsInRoundShop(CollectionsDB colDB)
        {
            // Parse the stored Query
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");
            SqlCommand FindRounds = new SqlCommand(File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "RoundsNotInShop.sql")));
            FindRounds.CommandTimeout = 90;

            // Get all Round Components from WaxRoundNew
            DataTable WaxRoundNew = colDB.RunQuery(FindRounds, "Collections");

            // Record errors for any rows where a Round does not exist in the roundshop
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (WaxRoundNew != null)
            {
                foreach (DataRow row in WaxRoundNew.Rows)
                {
                    erroneousDatums.Add(new ErroneousDatum(
                        "Multiple",
                        "CollectionID",
                        "Multiple - " + row["Properties"].ToString(),
                        "RoundName",
                        "WaxRoundNew",
                        row["RoundName"].ToString(),
                        "This round does not exist in the Round Shop."));
                }
            }

            // Return the list of errors
            return erroneousDatums;
        }
        #endregion

        /// <summary>
        /// Performs each and every data validation check.
        /// Writes reports on any erroneous data to the Shared Drive. 
        /// </summary>
        public List<List<string>> performAllChecks(InCabDB cabDB)
        {
            List<List<string>> ErrorsFound = new List<List<string>>();

            // Checks that the InCab Messages table has received at least one message today
            if (!CheckMessagesToday(cabDB))
                ErrorsFound.Add(new List<string> { cabDB.displayName, "No InCab Messages found today", "0", "SELECT * FROM [messages] WHERE Date = REPLACE(CONVERT(VARCHAR, GETDATE(),103),'/','')" });

            #region Schedule checks
            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Schedule Services for " + cabDB.displayName);
            List<ErroneousDatum> erroneousScheduleServices = checkScheduleServices(cabDB);
            if (erroneousScheduleServices.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousScheduleServices.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidScheduleServices_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousScheduleServices));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Schedule Services", erroneousScheduleServices.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Schedule Days for " + cabDB.displayName);
            List<ErroneousDatum> erroneousScheduleDays = checkScheduleDays(cabDB);
            if (erroneousScheduleDays.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousScheduleDays.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidScheduleDays_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousScheduleDays));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Schedule Days", erroneousScheduleDays.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Schedule Weeks for " + cabDB.displayName);
            List<ErroneousDatum> erroneousScheduleWeeks = checkScheduleWeeks(cabDB);
            if (erroneousScheduleWeeks.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousScheduleWeeks.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidScheduleWeeks_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousScheduleWeeks));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Schedule Weeks", erroneousScheduleWeeks.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Schedule Crews for " + cabDB.displayName);
            List<ErroneousDatum> erroneousScheduleCrews = checkScheduleCrews(cabDB);
            if (erroneousScheduleCrews.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousScheduleCrews.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidScheduleCrews_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousScheduleCrews));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Schedule Crews", erroneousScheduleCrews.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Schedule UPRNs for " + cabDB.displayName);
            List<ErroneousDatum> erroneousScheduleUPRNs = checkScheduleUPRNs(cabDB);
            if (erroneousScheduleUPRNs.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousScheduleUPRNs.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidScheduleUPRNs_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousScheduleUPRNs));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Schedule UPRNs", erroneousScheduleUPRNs.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }
            #endregion

            #region Itinerary Checks
            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Itinerary Services for " + cabDB.displayName);
            List<ErroneousDatum> erroneousItineraryServices = checkItineraryServices(cabDB);
            if (erroneousItineraryServices.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousItineraryServices.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidItineraryServices_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousItineraryServices));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Itinerary Services", erroneousItineraryServices.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Itinerary Days for " + cabDB.displayName);
            List<ErroneousDatum> erroneousItineraryDays = checkItineraryDays(cabDB);
            if (erroneousItineraryDays.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousItineraryDays.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidItineraryDays_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousItineraryDays));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Itinerary Days", erroneousItineraryDays.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Itinerary Weeks for " + cabDB.displayName);
            List<ErroneousDatum> erroneousItineraryWeeks = checkItineraryWeeks(cabDB);
            if (erroneousItineraryWeeks.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousItineraryWeeks.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidItineraryWeeks_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousItineraryWeeks));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Itinerary Weeks", erroneousItineraryWeeks.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }

            // Check for invalid Roundnames and write a report on any found
            Console.WriteLine("Checking Itinerary Crews for " + cabDB.displayName);
            List<ErroneousDatum> erroneousItineraryCrews = checkItineraryCrews(cabDB);
            if (erroneousItineraryCrews.Count > 0)
            {
                Console.WriteLine("\tFound: " + erroneousItineraryCrews.Count.ToString());
                // Create a unique filepath derived from URLName and DateTime
                string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\" +
                    cabDB.URLName + "_InvalidItineraryCrews_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";

                // Write the file to the Shared Drive
                File.WriteAllText(@filepath, createErrorReport(erroneousItineraryCrews));
                ErrorsFound.Add(new List<string> { cabDB.displayName, "Invalid Itinerary Crews", erroneousItineraryCrews.Count.ToString(), "<a href=\"" + filepath + "\">Report</a>" });
            }
            #endregion

            return ErrorsFound;
        }

        public bool CheckMessagesToday(InCabDB cabDB)
        {
            List<ErroneousDatum> errors = new List<ErroneousDatum>();

            SqlCommand CountMessages = new SqlCommand("SELECT COUNT(*) FROM [messages] WHERE Date = REPLACE(CONVERT(VARCHAR, GETDATE(),103),'/','')");
            string MessagesToday = cabDB.RunScalarQuery(CountMessages, "inCab");

            if (MessagesToday == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Validates each Service in dbo.InCabSchedule
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkScheduleServices(InCabDB cabDB)
        {
            // Get all services from InCabSchedule along with their primary key
            DataTable Schedule = cabDB.RunQuery(new SqlCommand("SELECT ID, Service, UPRN FROM InCabSchedule"), "InCab");

            // Validate each service and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Schedule != null)
            {
                foreach (DataRow row in Schedule.Rows)
                {
                    if (!validateInCabService(row["Service"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["ID"].ToString(),
                            "ID",
                            row["UPRN"].ToString(),
                            "Service",
                            "InCabSchedule",
                            row["Service"].ToString(),
                            "Service failed to validate."));
                }
            }

            // Return the primary keys of any Services which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Day in dbo.InCabSchedule
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkScheduleDays(InCabDB cabDB)
        {
            // Get all days from InCabSchedule along with their primary key
            DataTable Schedule = cabDB.RunQuery(new SqlCommand("SELECT ID, Day, UPRN FROM InCabSchedule"), "InCab");

            // Validate each day and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Schedule != null)
            {
                foreach (DataRow row in Schedule.Rows)
                {
                    if (!validateDay(row["Day"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["ID"].ToString(),
                            "ID",
                            row["UPRN"].ToString(),
                            "Day",
                            "InCabSchedule",
                            row["Day"].ToString(),
                            "Day failed to validate."));
                }
            }

            // Return the primary keys of any Days which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Week in dbo.InCabSchedule
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkScheduleWeeks(InCabDB cabDB)
        {
            // Get all weeks from InCabSchedule along with their primary key
            DataTable Schedule = cabDB.RunQuery(new SqlCommand("SELECT ID, Week, UPRN FROM InCabSchedule"), "InCab");

            // Validate each week and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Schedule != null)
            {
                foreach (DataRow row in Schedule.Rows)
                {
                    if (!validateInCabWeek(row["Week"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["ID"].ToString(),
                            "ID",
                            row["UPRN"].ToString(),
                            "Week",
                            "InCabSchedule",
                            row["Week"].ToString(),
                            "Week failed to validate."));
                }
            }

            // Return the primary keys of any Weeks which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Crew in dbo.InCabSchedule
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkScheduleCrews(InCabDB cabDB)
        {
            // Get all crews from InCabSchedule along with their primary key
            DataTable Schedule = cabDB.RunQuery(new SqlCommand("SELECT ID, Crew, UPRN FROM InCabSchedule"), "InCab");

            // Validate each crew and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Schedule != null)
            {
                foreach (DataRow row in Schedule.Rows)
                {
                    if (!validateInCabCrew(row["Crew"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["ID"].ToString(),
                            "ID",
                            row["UPRN"].ToString(),
                            "Crew",
                            "InCabSchedule",
                            row["Crew"].ToString(),
                            "Crew failed to validate."));
                }
            }

            // Return the primary keys of any Crews which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each UPRN in dbo.InCabSchedule
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkScheduleUPRNs(InCabDB cabDB)
        {
            // Get all UPRNs from InCabSchedule along with their primary key
            DataTable Schedule = cabDB.RunQuery(new SqlCommand("SELECT ID, UPRN FROM InCabSchedule"), "InCab");

            // Validate each UPRN and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Schedule != null)
            {
                foreach (DataRow row in Schedule.Rows)
                {
                    if (!validateUPRN(row["UPRN"].ToString()))
                        erroneousDatums.Add(new ErroneousDatum(
                            row["ID"].ToString(),
                            "ID",
                            row["UPRN"].ToString(),
                            "UPRN",
                            "InCabSchedule",
                            row["UPRN"].ToString(),
                            "UPRN failed to validate."));
                }
            }

            // Return the primary keys of any UPRNs which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Crew in dbo.InCabItinerary
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkItineraryCrews(InCabDB cabDB)
        {
            // Get all Crews from InCabItinerary along with their primary key
            DataTable Itinerary = cabDB.RunQuery(new SqlCommand("SELECT itineraryID, itineraryRound FROM InCabItinerary"), "InCab");

            // Validate each Crew and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Itinerary != null)
            {
                foreach (DataRow row in Itinerary.Rows)
                {
                    if (!validateInCabCrew(row["itineraryRound"].ToString())
                        && row["itineraryRound"].ToString().ToLower() != "update"
                        && row["itineraryRound"].ToString().ToLower() != "incabdiag")
                        erroneousDatums.Add(new ErroneousDatum(
                            row["itineraryID"].ToString(),
                            "itineraryID",
                            "NA",
                            "itineraryRound",
                            "InCabItinerary",
                            row["itineraryRound"].ToString(),
                            "itineraryRound failed to validate."));
                }
            }

            // Return the primary keys of any Crews which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Service in dbo.InCabItinerary
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkItineraryServices(InCabDB cabDB)
        {
            // Get all Services from InCabItinerary along with their primary key
            DataTable Itinerary = cabDB.RunQuery(new SqlCommand("SELECT itineraryID, itineraryService FROM InCabItinerary"), "InCab");

            // Validate each Service and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Itinerary != null)
            {
                foreach (DataRow row in Itinerary.Rows)
                {
                    if (!validateInCabService(row["itineraryService"].ToString())
                        && row["itineraryService"].ToString().ToLower() != "update"
                        && row["itineraryService"].ToString().ToLower() != "incabdiag")
                        erroneousDatums.Add(new ErroneousDatum(
                            row["itineraryID"].ToString(),
                            "itineraryID",
                            "NA",
                            "itineraryService",
                            "InCabItinerary",
                            row["itineraryService"].ToString(),
                            "Service failed to validate."));
                }
            }

            // Return the primary keys of any Services which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Day in dbo.InCabItinerary
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkItineraryDays(InCabDB cabDB)
        {
            // Get all Days from InCabItinerary along with their primary key
            DataTable Itinerary = cabDB.RunQuery(new SqlCommand("SELECT itineraryID, itineraryDay FROM InCabItinerary"), "InCab");

            // Validate each Day and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Itinerary != null)
            {
                foreach (DataRow row in Itinerary.Rows)
                {
                    if (!validateDay(row["itineraryDay"].ToString())
                        && row["itineraryDay"].ToString().ToLower() != "update"
                        && row["itineraryDay"].ToString().ToLower() != "incabdiag")
                        erroneousDatums.Add(new ErroneousDatum(
                            row["itineraryID"].ToString(),
                            "itineraryID",
                            "NA",
                            "itineraryDay",
                            "InCabItinerary",
                            row["itineraryDay"].ToString(),
                            "Day failed to validate."));
                }
            }

            // Return the primary keys of any Days which failed validation
            return erroneousDatums;
        }

        /// <summary>
        /// Validates each Week in dbo.InCabItinerary
        /// </summary>
        /// <returns></returns>
        public List<ErroneousDatum> checkItineraryWeeks(InCabDB cabDB)
        {
            // Get all Weeks from InCabItinerary along with their primary key
            DataTable Itinerary = cabDB.RunQuery(new SqlCommand("SELECT itineraryID, itineraryWeek FROM InCabItinerary"), "InCab");

            // Validate each Week and store the Primary Keys of any which fail validation
            List<ErroneousDatum> erroneousDatums = new List<ErroneousDatum>();
            if (Itinerary != null)
            {
                foreach (DataRow row in Itinerary.Rows)
                {
                    if (!validateInCabWeek(row["itineraryWeek"].ToString())
                        && row["itineraryWeek"].ToString().ToLower() != "update"
                        && row["itineraryWeek"].ToString().ToLower() != "incabdiag")
                        erroneousDatums.Add(new ErroneousDatum(
                            row["itineraryID"].ToString(),
                            "itineraryID",
                            "NA",
                            "itineraryWeek",
                            "InCabItinerary",
                            row["itineraryWeek"].ToString(),
                            "Week failed to validate."));
                }
            }

            // Return the primary keys of any Weeks which failed validation
            return erroneousDatums;
        }

        public void DeletePreviousReports()
        {
            string filepath = @"\\192.168.0.136\Shared\Automation_Reports\Azure_Monitoring\";
            Console.WriteLine("Deleting all reports found in: " + filepath);
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(filepath);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                Console.WriteLine("Finished deleting.");
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Error encountered whilst deteting previous reports.");
                Console.WriteLine(Ex);
                Console.WriteLine(Ex.Message);
            }
        }
    }
}
