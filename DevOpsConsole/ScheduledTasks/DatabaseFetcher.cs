﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Webaspx.Database;

namespace DevOpsConsole.ScheduledTasks
{
    class DatabaseFetcher : ScheduledTask
    {
        private static List<String> listOfErrors = new List<String>();
        private static List<String> backUpFailuresList = new List<String>();
        private static List<String> databaseSizeWarningList = new List<String>();
        private static String users, properties, bins, dbSize, dbMax, messages, dbProp;
        private static int FAILED_ATTEMPTS_MAX = 30;
        private static double DATABASE_GETTING_FULL_PROPORTION = 0.8;

        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public DatabaseFetcher()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Database Fetcher";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            string[,] inCabArray = FetchTheInCabDatabaseData();
            WriteOutFile(inCabArray, "Incab Databases Summary");

            string[,] collectionsArray = FetchTheCollectionsDatabaseData();
            WriteOutFile(collectionsArray, "Collections Databases Summary");

            string output = "";
            if (listOfErrors.Count > 0)
            {
                output += "<h3>SQL Query Errors</h3><pre>";
                foreach (string error in listOfErrors)
                {
                    output += error + "\n";
                }
                output += "</pre>";
            }

            if (backUpFailuresList.Count > 0)
            {
                output += "<h3>Backup Failures</h3><pre>";
                foreach (string failure in backUpFailuresList)
                {
                    output += failure + "\n";
                }
                output += "</pre>";
            }

            if (databaseSizeWarningList.Count > 0)
            {
                output += "<h3>Database Size Warnings</h3><pre>";
                foreach (string warning in databaseSizeWarningList)
                {
                    output += warning + "\n";
                }
                output += "</pre>";
            }

            if (output != "")
            {
                SendEmail(output, "Data Fetcher", recipients);
            }
        }

        private string[,] FetchTheInCabDatabaseData()
        {
            string[,] inCabArray;

            List<InCabDB> InCabClients = getAllInCabDBs().Where(p => p.Status == "Live").ToList();
            int FAILED_ATTEMPTS_MAX = 30;

            int numberOfSQLQueries = 4;
            inCabArray = new String[numberOfSQLQueries + 3, InCabClients.Count + 1];

            inCabArray[1, 0] = "users";
            inCabArray[2, 0] = "messages";
            inCabArray[3, 0] = "dbSize";
            inCabArray[4, 0] = "dbMax";
            inCabArray[5, 0] = "proportionUsed";
            inCabArray[6, 0] = "backedup?";
            inCabArray[0, 0] = DateTime.Now.ToString("yyyy-MM-dd-HHmm");

            for (int i = 0; i < InCabClients.Count; i++)//for place in the list of incab clients
            {
                inCabArray[6, i + 1] = CheckBackUps(InCabClients[i].Name, "incab") ? "Y" : "N";

                users = messages = dbMax = dbSize = dbProp = "XXXXX";
                inCabArray[0, i + 1] = InCabClients[i].Name;

                Console.WriteLine("Fetching Azure INCAB data for " + InCabClients[i].Name + ".");

                if (InCabClients[i].Name != null) //check that there is a council in this position (not been removed)
                {
                    int failedAttempts = 0;
                    bool connectionWorked = false;

                    while (failedAttempts < FAILED_ATTEMPTS_MAX)
                    {
                        try
                        {
                            using (DatabaseConnection dc = new DatabaseConnection(InCabClients[i].Name, Catalog.Incab))
                            {
                                //RunTruckCheckforMike(dc, incabClientsList[i].WorkingDays, incabClientsList[i].Name); //vehicles that haven't been seen for 3 days
                                InCabSQLQueries(dc); //#users, #messages, dbsize etc
                                connectionWorked = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            failedAttempts++;
                            StoreListOfErrors(ex, failedAttempts, i, "incab", InCabClients[i].Name);
                        }

                        inCabArray[1, i + 1] = users;
                        inCabArray[2, i + 1] = messages;
                        inCabArray[3, i + 1] = dbSize;
                        inCabArray[4, i + 1] = dbMax;
                        inCabArray[5, i + 1] = dbProp;

                        failedAttempts = (connectionWorked) ? FAILED_ATTEMPTS_MAX : failedAttempts;

                    }
                }
            }
            return inCabArray;
        } //INCAB: database stats

        private string[,] FetchTheCollectionsDatabaseData()//COLLECTIONS: database stats
        {
            string[,] collectionsArray;

            List<CollectionsDB> CollectionsClients = getAllColDBs().Where(p => p.Status == "Live").ToList();

            int numOfSQLQueries = 5;
            collectionsArray = new String[numOfSQLQueries + 3, CollectionsClients.Count + 1];

            collectionsArray[1, 0] = "users";
            collectionsArray[2, 0] = "properties";
            collectionsArray[3, 0] = "bins";
            collectionsArray[4, 0] = "dbSize";
            collectionsArray[5, 0] = "dbMax";
            collectionsArray[6, 0] = "proportionUsed";
            collectionsArray[7, 0] = "backedup?";
            collectionsArray[0, 0] = DateTime.Now.ToString("yyyy-MM-dd-HHmm");
            for (int i = 0; i < CollectionsClients.Count; i++)
            {
                collectionsArray[7, i + 1] = CheckBackUps(CollectionsClients[i].Name, "Collections") ? "Y" : "N";

                users = properties = bins = dbSize = dbMax = dbProp = "XXXXX";
                collectionsArray[0, i + 1] = CollectionsClients[i].Name;

                //bwDatabaseSQL.ReportProgress(0, "Fetching Azure COLLECTIONS data for " + collectionsClientsList[i].Name + ".");

                if (CollectionsClients[i].Name != null) //check that there is a council in this position (not been removed)
                {
                    int failedAttempts = 0;
                    bool connectionWorked = false;

                    while (failedAttempts < FAILED_ATTEMPTS_MAX)
                    {
                        try
                        {
                            using (DatabaseConnection dc = new DatabaseConnection(CollectionsClients[i].Name, Catalog.Collections))
                            {
                                CollectionsSQLQueries(dc);
                                connectionWorked = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            failedAttempts++;
                            StoreListOfErrors(ex, failedAttempts, i, "collections", CollectionsClients[i].Name);
                        }

                        collectionsArray[1, i + 1] = users;
                        collectionsArray[2, i + 1] = properties;
                        collectionsArray[3, i + 1] = bins;
                        collectionsArray[4, i + 1] = dbSize;
                        collectionsArray[5, i + 1] = dbMax;
                        collectionsArray[6, i + 1] = dbProp;

                        failedAttempts = (connectionWorked) ? FAILED_ATTEMPTS_MAX : failedAttempts;
                    }
                }
            }
            return collectionsArray;
        }

        private static bool CheckBackUps(string clientName, string inCabOrCollections)
        {
            DateTime y = DateTime.Today.AddDays(-1);
            string yYearDate, yMonthDate, yDayDate;

            yYearDate = y.ToString("yyyy");
            yMonthDate = y.ToString("MM");
            yDayDate = y.ToString("dd");

            //eg W:\SouthLanarkshire\Collections
            //eg 2018-03-01--17_LLPG.csv

            string filePath = @"W:\" + clientName + @"\" + inCabOrCollections + @"\";
            string fileName = yYearDate + "-" + yMonthDate + "-" + yDayDate;

            //create list of acceptable file names - last 12 hours
            List<string> validDateHours = new List<string>();
            //String acceptableDateHoursRange = "";
            for (int h = 0; h < 23; h++)
            {
                validDateHours.Add(DateTime.Now.AddHours(-h).ToString("yyyy-MM-dd--HH"));
                //acceptableDateHoursRange += DateTime.Now.AddHours(-h).ToString("yyyy-MM-dd--HH")+"#";
            }

            if (Directory.Exists(filePath))
            {
                //String[] backedupFile = Directory.GetFiles(filePath).Where(x => x.Contains(fileName)).ToArray();
                string[] backedupFiles = Directory.GetFiles(filePath).ToArray();
                bool backUpExists = false;
                for (int b = 0; b < backedupFiles.Length; b++)
                {
                    foreach (var item in validDateHours)
                    {
                        if (backedupFiles[b].Contains(item))
                        {
                            backUpExists = true;
                            break;
                        }
                    }

                }

                if (backUpExists && clientName != "Webaspx")
                    return true;
                else
                {
                    backUpFailuresList.Add(clientName + " " + inCabOrCollections);
                    return false;
                }
            }
            else
            {
                backUpFailuresList.Add(clientName + " " + inCabOrCollections);
                return false;
            }
        }

        private static void InCabSQLQueries(DatabaseConnection dc)
        {
            Console.WriteLine("Fetching Azure data for " + dc.Connection.Database + " - Users");
            users = dc.ExecuteScalar("Select Count(*) as 'Users' from Users where Email not like '%webaspx.com'").ToString();
            Console.WriteLine("Fetching Azure data for " + dc.Connection.Database + " - messages");
            //messages = dc.ExecuteScalar("Select Count(id) as 'Messages' from Messages").ToString();
            messages = dc.ExecuteScalar("SELECT row_count FROM sys.dm_db_partition_stats WHERE object_id=OBJECT_ID('Messages') AND (index_id=0 or index_id=1);").ToString();
            DatabaseSizeQueries(dc);
        }

        private static void CollectionsSQLQueries(DatabaseConnection dc)
        {
            Console.WriteLine("Fetching Azure data for " + dc.Connection.Database + " - Users");
            users = dc.ExecuteScalar("Select Count(*) as 'Users' from Users where Email not like '%webaspx.com%' and email not like 'live.webaspx%'").ToString();

            Console.WriteLine("Fetching Azure data for " + dc.Connection.Database + " - Properties");
            properties = dc.ExecuteScalar("Select Count(WaxID) as 'Properties' from LLPG").ToString();

            Console.WriteLine("Fetching Azure data for " + dc.Connection.Database + " - Bins");
            bins = dc.ExecuteScalar("Select Count(ID) as 'Bins' from Bins").ToString();

            DatabaseSizeQueries(dc);
        }

        private static void DatabaseSizeQueries(DatabaseConnection dc)
        {
            dbSize = dc.ExecuteScalar("Select (SUM(reserved_page_count) * 8192)/104856.0 AS [Database Size in MB] FROM sys.dm_db_partition_stats").ToString();

            String dbMaxQuery = "Select Convert(BIGINT,DATABASEPROPERTYEX('" + dc.Connection.Database + "', 'MaxSizeInBytes'))/1024/1024 AS 'Maxsize in GB'";

            dbMax = dc.ExecuteScalar(dbMaxQuery).ToString();

            dbProp = (Convert.ToDouble(dbSize) / 10.0 / Convert.ToDouble(dbMax)).ToString();

            DbSizeWarningCheck(dbProp, dc.Connection.Database.ToString());
        }

        private static void DbSizeWarningCheck(String dbProp, String databaseName)//check that the database proportion used is at an acceptable level
        {
            if (Convert.ToDouble(dbProp) > DATABASE_GETTING_FULL_PROPORTION)
            {
                databaseSizeWarningList.Add(databaseName + " " + dbProp.Substring(0, 4));
            }
        }

        /// <summary>
        /// Make a list of the things that have gone wrong with details about them
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="failedAttempts"></param>
        /// <param name="i"></param>
        /// <param name="incabCollections"></param>
        /// <param name="clientsName"></param>
        private static void StoreListOfErrors(Exception ex, int failedAttempts, int i, String incabCollections, String clientsName)
        {
            string errorMessage = ex.Message.ToString();
            string lineNumber = "";

            if (ex.StackTrace.ToString().Length > 0 && failedAttempts == FAILED_ATTEMPTS_MAX)
            {
                lineNumber = ex.StackTrace.Split(' ').Last();
                errorMessage += " | Line: " + lineNumber + " | " + FAILED_ATTEMPTS_MAX + " attempts were made to connect.";
                listOfErrors.Add(incabCollections + " " + clientsName + " | " + errorMessage + " | " + DateTime.Now);
            }
        }


        private static void WriteOutFile(String[,] arrayToPrint, String fileName)//prints out an array
        {
            if (arrayToPrint == null)
                return;

            String fileLocation = @"\\192.168.0.136\Shared\Azure Monitoring\Automated 2018\" + fileName + ".csv";

            using (StreamWriter writer = File.AppendText(fileLocation))
            {
                for (int i = 0; i < arrayToPrint.GetLength(0); i++)
                {
                    String outline = "";
                    for (int j = 0; j < arrayToPrint.GetLength(1); j++)
                    {
                        outline += arrayToPrint[i, j] + ",";
                    }
                    writer.WriteLine(outline);
                }
                writer.WriteLine();
            }
        }
    }
}
