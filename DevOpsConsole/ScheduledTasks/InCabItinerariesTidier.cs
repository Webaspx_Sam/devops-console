﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class InCabItinerariesTidier : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public InCabItinerariesTidier()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "InCab Itineraries Tidier";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Removing all but the latest 2 copies of each distinct Itinerary:");
            List<InCabDB> InCabClients = getAllInCabDBs();

            foreach (InCabDB cabDB in InCabClients)
            {
                Console.WriteLine("Tidying Itineraries for " + cabDB.displayName + ":");
                TidyItineraries(cabDB);
            }
        }

        private void TidyItineraries(InCabDB cabDB)
        {
            // Create a set of each different itinerary in the table
            SqlCommand getDistinctItineraries = new SqlCommand("SELECT DISTINCT itineraryRound, itineraryService, itineraryDay, itineraryWeek FROM IncabItinerary WHERE itineraryRound != 'update' AND itineraryRound != 'InCabDiag'");
            DataTable distinctItineraries = cabDB.RunQuery(getDistinctItineraries, "InCab");

            if (distinctItineraries == null)
            {
                Console.WriteLine("No itineraries found in IncabItinerary");
                return; // Finish early if InCabItinerary table was empty
            }

            // Otherwise analyse each unique Itinerary in turn
            foreach (DataRow itinerary in distinctItineraries.Rows)
            {
                string Day = itinerary["itineraryDay"].ToString();
                string Week = itinerary["itineraryWeek"].ToString();
                string Service = itinerary["itineraryService"].ToString();
                string Round = itinerary["itineraryRound"].ToString();

                Console.Write("Getting entries for " + Day + " Wk" + Week + " " + Service + " " + Round + " " + cabDB.displayName + "...");

                // Gather a list of all entries for this itinerary by ID DESC so newest two will be first two returned
                SqlCommand getItineraryHistory = new SqlCommand("SELECT * FROM InCabItinerary WHERE itineraryRound = @ROUND " +
                    "AND itineraryDay = @DAY AND itineraryService = @SERVICE AND itineraryWeek = @Week " +
                    "ORDER BY itineraryID DESC");
                getItineraryHistory.Parameters.Add(new SqlParameter("ROUND", Round));
                getItineraryHistory.Parameters.Add(new SqlParameter("DAY", Day));
                getItineraryHistory.Parameters.Add(new SqlParameter("WEEK", Week));
                getItineraryHistory.Parameters.Add(new SqlParameter("SERVICE", Service));
                DataTable itineraryHistory = cabDB.RunQuery(getItineraryHistory, "InCab");

                Console.WriteLine(itineraryHistory.Rows.Count.ToString() + " found.");

                // If there are more than 2 we want to delete the 3rd, 4th .. etc.
                if (itineraryHistory.Rows.Count > 2)
                {
                    // Iterate over rows starting at the 3rd and delete each by ID
                    for (int i = 2; i < itineraryHistory.Rows.Count; i++)
                    {
                        SqlCommand deleteItinerary = new SqlCommand("DELETE FROM InCabItinerary WHERE itineraryID = @ID");
                        deleteItinerary.Parameters.Add(new SqlParameter("ID", itineraryHistory.Rows[i]["itineraryID"].ToString()));

                        Console.WriteLine("\tDeleting entry with itineraryID " + itineraryHistory.Rows[i]["itineraryID"].ToString());

                        cabDB.RunNonQuery(deleteItinerary, "InCab");
                    }
                }
            }
        }
    }
}
