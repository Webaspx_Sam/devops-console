﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using Webaspx.Database;

namespace DevOpsConsole.ScheduledTasks
{
    class LoginMonitor : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public LoginMonitor()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Login Monitor";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            // Create a list of all active InCab databases
            List<InCabDB> InCabClients = getAllInCabDBs();

            Console.WriteLine("Gathering a list of recent Portal Logins...");

            string TableOfRecentPortalLogins = GetRecentPortalLogins();
            string TableOfLast100DaysTruckLogins = PopulateLast100DaysLogIns(InCabClients);

            SendEmail(TableOfRecentPortalLogins + TableOfLast100DaysTruckLogins, "Recent Login Trawler", recipients);
        }

        public string GetRecentPortalLogins()
        {
            List<LoginsRecord> InCabLogins = GetInCabLogins(getAllInCabDBs());
            List<LoginsRecord> CollectionsLogins = GetCollectionsLogins(getAllColDBs());

            string output = "<h3>Incab Portal Logins</h3>";
            output += "<table border='1' style='border: 1; border-color: #f7b391'>" +
                "<tr style='background-color: #1e3663'>" +
                "<th>Client</th><th>Today</th><th>Within 7 days</th><th>Over a week</th><th>Never</th>";
            foreach (LoginsRecord record in InCabLogins)
            {
                output += "<tr><td>" + record.Client + "</td><td>" + record.Daily +
                    "</td><td>" + record.Weekly + "</td><td>" + record.Monthly +
                    "</td><td>" + record.Never + "</td></tr>";
            }
            output += "</table>";

            output += "<h3>Collections Portal Logins</h3>";
            output += "<table border='1' style='border: 1; border-color: #f7b391'>" +
                "<tr style='background-color: #1e3663'>" +
                "<th>Client</th><th>Today</th><th>Within 7 days</th><th>Over a week</th><th>Never</th>";
            foreach (LoginsRecord record in CollectionsLogins)
            {
                output += "<tr><td>" + record.Client + "</td><td>" + record.Daily +
                    "</td><td>" + record.Weekly + "</td><td>" + record.Monthly +
                    "</td><td>" + record.Never + "</td></tr>";
            }
            output += "</table>";

            return output;
        }

        public static List<LoginsRecord> GetInCabLogins(List<InCabDB> incabClients)
        {
            List<LoginsRecord> InCabLogins = new List<LoginsRecord>();

            foreach (InCabDB cabDB in incabClients)
            {
                Console.WriteLine("Finding Logins for " + cabDB.displayName + " InCab:");
                DataTable Users = cabDB.RunQuery(new SqlCommand("SELECT Email, SUBSTRING([Session], 1, 8) AS DateLoggedIn FROM Users"), "InCab");

                if (Users != null)
                {
                    LoginsRecord clientRecord = new LoginsRecord();
                    clientRecord.Client = cabDB.displayName;
                    clientRecord.PortalType = "InCab";

                    foreach (DataRow row in Users.Rows)
                    {
                        int daysSinceLoggedIn = -1; // Will be read as 'never'
                        if (row["DateLoggedIn"].ToString() != "")
                        {
                            DateTime userDateFormated = DateTime.ParseExact(row["DateLoggedIn"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
                            daysSinceLoggedIn = (DateTime.Now - userDateFormated).Days;
                        }

                        if (daysSinceLoggedIn == -1)
                        {
                            Console.WriteLine(row["Email"].ToString() + " has never logged in.");
                            clientRecord.Never++;
                        }
                        else if (daysSinceLoggedIn < 1)
                        {
                            clientRecord.Daily++;
                            Console.WriteLine(row["Email"].ToString() + " has logged in within a day.");
                        }
                        else if (daysSinceLoggedIn < 7)
                        {
                            clientRecord.Weekly++;
                            Console.WriteLine(row["Email"].ToString() + " has logged in within a week.");
                        }
                        else if (daysSinceLoggedIn < 28)
                        {
                            clientRecord.Monthly++;
                            Console.WriteLine(row["Email"].ToString() + " has logged in within a month.");
                        }
                    }
                    InCabLogins.Add(clientRecord);
                }
            }

            return InCabLogins;
        }

        public static List<LoginsRecord> GetCollectionsLogins(List<CollectionsDB> collectionsClients)
        {
            List<LoginsRecord> CollectionsLogins = new List<LoginsRecord>();

            foreach (CollectionsDB colDB in collectionsClients)
            {
                Console.WriteLine("Finding Logins for " + colDB.displayName + " Collections:");
                DataTable Users = colDB.RunQuery(new SqlCommand("SELECT Email, SUBSTRING([Session], 1, 8) AS DateLoggedIn FROM Users"), "Collections");

                if (Users != null)
                {
                    LoginsRecord clientRecord = new LoginsRecord();
                    clientRecord.Client = colDB.displayName;
                    clientRecord.PortalType = "Collections";

                    foreach (DataRow row in Users.Rows)
                    {
                        int daysSinceLoggedIn = -1; // Will be read as 'never'
                        if (row["DateLoggedIn"].ToString() != "")
                        {
                            DateTime userDateFormated = DateTime.ParseExact(row["DateLoggedIn"].ToString(), "ddMMyyyy", CultureInfo.InvariantCulture);
                            daysSinceLoggedIn = (DateTime.Now - userDateFormated).Days;
                        }

                        if (daysSinceLoggedIn == -1)
                        {
                            Console.WriteLine(row["Email"].ToString() + " has never logged in.");
                            clientRecord.Never++;
                        }
                        else if (daysSinceLoggedIn < 1)
                        {
                            clientRecord.Daily++;
                            Console.WriteLine(row["Email"].ToString() + " has logged in within a day.");
                        }
                        else if (daysSinceLoggedIn < 7)
                        {
                            clientRecord.Weekly++;
                            Console.WriteLine(row["Email"].ToString() + " has logged in within a week.");
                        }
                        else if (daysSinceLoggedIn < 28)
                        {
                            clientRecord.Monthly++;
                            Console.WriteLine(row["Email"].ToString() + " has logged in within a month.");
                        }
                    }
                    CollectionsLogins.Add(clientRecord);
                }
            }

            return CollectionsLogins;
        }



        private static string PopulateLast100DaysLogIns(List<InCabDB> InCabClients)
        {
            var councilSuccessList = new List<string>();
            for (int i = 0; i < InCabClients.Count; i++)
            {
                Console.WriteLine("Getting Login Messages for Incab (" + InCabClients[i].Name + ")");
                List<string[]> incabLogInRows = new List<string[]>();
                List<int> origIDs = new List<int>();
                int timeOut = 240;
                using (var dbc = new DatabaseConnection(InCabClients[i].Name, Catalog.Incab, timeOut))
                {
                    //read incab database
                    //lblCurrentAction.Text = "Reading Incab database for Login Messages : "+InCabClients[i].Name;
                    //Application.DoEvents();
                    //get 100 days ago
                    string oneHundredDaysAgo = DateTime.Now.AddDays(-100).ToString("yyyyMMdd");
                    string sqlRead = "SELECT messageID, date, time1, user1, id " +
                        "FROM messages " +
                        "WHERE messageid LIKE '$IN%' " +
                        "AND ID >= (SELECT MessStartID FROM IncabDateId WHERE DateYYYYMMDD='" + oneHundredDaysAgo + "')";
                    int rowsInFromDB = 0;
                    using (var reader = dbc.ExecuteReader(sqlRead))
                    {
                        //lblCurrentAction.Text = "Processing Login messages of Last 100 Days for Login Messages : " + InCabClients[i].Name;
                        //Application.DoEvents();                        
                        //while (reader.HasRows)

                        while (reader.Read())
                        {
                            rowsInFromDB++;
                            if (rowsInFromDB % 200 == 0)
                            {
                                Console.WriteLine("Reading InCab Login msgs (" + InCabClients[i].Name + ") " + rowsInFromDB + " rows");
                            }
                            string messageID = reader["messageid"].ToString(); ;
                            string[] messageIDSplit = messageID.Replace("$IN$", "").Split(':');
                            string reg = messageIDSplit[0];
                            string driveID = messageIDSplit[1];
                            string serv3L = messageIDSplit[2];
                            string dayDDD = messageIDSplit[3];
                            string week = messageIDSplit[4].Replace("Week", "").Replace(" ", "").Trim(); ;
                            string Crew = messageIDSplit[5];
                            string swVers = "";
                            string itinDate = "";
                            if (messageIDSplit.Length > 6)
                            {
                                swVers = messageIDSplit[6];
                            }
                            if (messageIDSplit.Length > 7)
                            {
                                itinDate = messageIDSplit[7];
                            }
                            string dateDDMMYYYY = reader["date"].ToString();
                            string time1 = reader["time1"].ToString();
                            string user1 = reader["user1"].ToString().Replace("Base", "").Replace(InCabClients[i].Name, "");
                            string[] rowContents = new string[11];
                            rowContents[0] = dateDDMMYYYY.Substring(4, 4) + dateDDMMYYYY.Substring(2, 2) + dateDDMMYYYY.Substring(0, 2);
                            rowContents[1] = Crew;
                            rowContents[2] = dayDDD;
                            rowContents[3] = week;
                            rowContents[4] = serv3L;
                            rowContents[5] = reg;
                            rowContents[6] = driveID;
                            rowContents[7] = time1;
                            rowContents[8] = itinDate;
                            rowContents[9] = swVers;
                            rowContents[10] = "Cr" + Crew + " " + dayDDD + " Wk" + week + " " + serv3L;

                            bool duplicateFound = false;
                            for (int r = 0; r < incabLogInRows.Count; r++)
                            {
                                if (incabLogInRows[r].SequenceEqual(rowContents))
                                {
                                    duplicateFound = true;
                                    break;
                                }
                            }
                            if (!duplicateFound)
                            {
                                incabLogInRows.Add(rowContents);
                                origIDs.Add(Convert.ToInt32(reader["id"]));
                            }
                        }
                    }
                    //write these distinct entries to database
                    //step through row in the listy writing each to the datbase
                    //remove duplicates
                    //incabLogInRows = incabLogInRows.Distinct().ToList();
                    Console.WriteLine("Writing Login Messages for Incab (" + InCabClients[i].Name + ") " + incabLogInRows.Count + " rows");
                    dbc.ExecuteNonQuery("Truncate Table InCabLogins");
                    int noOfRows = 0;
                    for (int w = 0; w < incabLogInRows.Count; w++)
                    {
                        //                                                   0       1     2       3       4   5       6       7           8               9       10
                        string sqlInsert = "Insert into InCabLogIns (DateYYYYMMDD, Crew, DayDDD, WeekNo, Serv3L, Reg, DriverID, DigTime, itinDateDDMMYYYY, SWVers,CollectionsRoundName)VALUES (";
                        sqlInsert += "'" + incabLogInRows[w][0] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][1] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][2] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][3] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][4] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][5] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][6] + "', ";
                        sqlInsert += "" + incabLogInRows[w][7] + ", ";
                        sqlInsert += "'" + incabLogInRows[w][8].Replace("\r", "") + "', ";
                        sqlInsert += "'" + incabLogInRows[w][9] + "', ";
                        sqlInsert += "'" + incabLogInRows[w][10] + "'";
                        sqlInsert += ");";
                        try
                        {
                            var count = dbc.ExecuteNonQuery(sqlInsert);
                            if (count != null)
                            {
                                noOfRows++;
                                if (w % 50 == 0)
                                {
                                    Console.WriteLine("Writing InCab Login msgs (" + InCabClients[i].Name + ") " + w + "/" + incabLogInRows.Count + " rows");
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            councilSuccessList.Add(InCabClients[i].Name + ":Error - ID(" + origIDs[w] + ")" + Ex.Message.ToString());
                        }
                    }
                    councilSuccessList.Add(InCabClients[i].Name + ":" + noOfRows + "/" + incabLogInRows.Count);
                }
            }
            Console.WriteLine("Finished Writing InCab Login msgs");

            string output = "<h3>InCab Login Messages Written to IncabLogins</h3><pre>";
            foreach (string line in councilSuccessList)
            {
                output += line + "\n";
            }
            output += "</pre>";
            return output;
        }
    }

    public class LoginsRecord
    {
        public string Client { get; set; }
        public string PortalType { get; set; }
        public int Daily { get; set; } = 0;
        public int Weekly { get; set; } = 0;
        public int Monthly { get; set; } = 0;
        public int Never { get; set; } = 0;
    }
}
