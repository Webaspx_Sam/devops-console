﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class RhonddaDepotManager : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public RhonddaDepotManager()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Rhondda Depot Manager";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Updating Rhondda's RoundShop Depot Column according to their RoundName...");

            // Create an instance of Richmondshire's Collections Database class 
            List<CollectionsDB> CollectionsClients = getAllColDBs();
            CollectionsDB Rhondda = CollectionsClients.Where(db => db.URLName == "rhondda").First();

            // Set the Depot column in RoundShop according to the RoundName
            ApplyRhonddaDepotsToRoundShop(Rhondda);
        }

        /// <summary>
        /// Updates Depot as per Rhondda's unique RoundNames. 
        /// Rounds beginning with Rhondda should have Dinas as their Depot
        /// Rounds beginning with Cynon should have Ty Amgen as their Depot
        /// Rounds beginning with Taff should have Ty Glantaff as their Depot
        /// </summary>
        /// <param name="Rhondda"></param>
        public static void ApplyRhonddaDepotsToRoundShop(CollectionsDB Rhondda)
        {
            SqlCommand FixRoundShop = new SqlCommand(
                "UPDATE RoundShop SET Depot = 'Dinas' WHERE RoundName LIKE 'Rhondda%'; " +
                "UPDATE RoundShop SET Depot = 'Ty Amgen' WHERE RoundName LIKE 'Cynon%'; " +
                "UPDATE RoundShop SET Depot = 'Ty Glantaff' WHERE RoundName LIKE 'Taff%';");

            Rhondda.RunNonQuery(FixRoundShop, "Collections");
        }
    }
}
