﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DevOpsConsole.ScheduledTasks
{
    class RichmondshireDateExtender : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public RichmondshireDateExtender()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Richmondshire Date Extender";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Extending Richmondshire's Garden Bin End Dates from 31/12/2019 to 31/01/2020...");

            // Create an instance of Richmondshire's Collections Database class 
            List<CollectionsDB> CollectionsClients = getAllColDBs();
            CollectionsDB Richmondshire = CollectionsClients.Where(db => db.URLName == "richmondshire").First();

            // Extend any bins with an end date of Dec 2019 to Jan 2020
            ExtendBinEndDates(Richmondshire, "Garden", "31/12/2019", "31/01/2020");
        }

        /// <summary>
        /// Updates the End_Date field of entries in the Bin Table. Matches on service and oldDate, updates to newDate.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="oldDate"></param>
        /// <param name="newDate"></param>
        public static void ExtendBinEndDates(CollectionsDB Richmondshire, string service, string oldDate, string newDate)
        {
            // Extract Bins from the Bins Table Where End Date matches oldDate
            DataTable BinsToExtend = Richmondshire.RunQuery(new SqlCommand("SELECT UPRN, Bin_Number, End_Date from Bins where End_Date='" + oldDate + "';"), "Collections");

            // End early if no bins found
            if (BinsToExtend == null)
                return;

            // For Each Bin:
            foreach (DataRow row in BinsToExtend.Rows)
            {
                // Change its EndDate to newDate
                SqlCommand ExtendBin = new SqlCommand(String.Format("UPDATE Bins SET End_Date='" + newDate + "' where UPRN='" + row["UPRN"].ToString() + "'and Bin_Number like '%" + service + "%' and End_Date='" + oldDate + "'"));
                Richmondshire.RunNonQuery(ExtendBin, "Collections");

                // Record the change we made in the History table
                SqlCommand RecordChange = new SqlCommand(String.Format("INSERT INTO [History] ([UPRN],[ChangeType], [WhatChanged], [ChangeDate], [ChangeUser])" +
                                            " VALUES ('" + row["UPRN"].ToString() + "', 'Extended End Date', '" + oldDate + " to " + newDate + "', GetDate(), 'WebaspxDataValidator');"));
                Richmondshire.RunNonQuery(RecordChange, "Collections");
            }
        }
    }
}