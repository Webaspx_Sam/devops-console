﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class RoundsItineraryMatcher : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public RoundsItineraryMatcher()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "Rounds and Itineraries Matcher";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            // Gather a list of Live InCab and Collections Databases
            List<InCabDB> InCabClients = getAllInCabDBs().Where(p => p.Status == "Live").ToList();
            List<CollectionsDB> CollectionsClients = getAllColDBs().Where(p => p.Status == "Live").ToList();
            Console.WriteLine("Checking that each Round in Collections has a matching Itinerary in InCab...");

            List<string> listItinerariesNotFound = FindRoundsWithoutItineraries(InCabClients, CollectionsClients);
            if (listItinerariesNotFound.Count > 0)
            {
                Console.WriteLine("Found " + listItinerariesNotFound.Count.ToString() + " Itinerary-less Rounds:");

                string emailBody = "<h3>Rounds without Itineraries</h3><ul>";
                foreach (string itin in listItinerariesNotFound)
                {
                    emailBody += "<li>" + itin + "</li>";
                    Console.WriteLine(itin);
                }
                emailBody += "</ul>";
                Console.WriteLine("Sending e-mail...");
                SendEmail(emailBody, "Rounds without Itineraries", recipients);
            }
        }

        /// <summary>
        /// Returns a list of rounds which exist in WaxRoundNew but don't have matching itineraries in InCabItinerary
        /// </summary>
        /// <param name="InCabClients">A list of InCab Databases</param>
        /// <param name="CollectionsClients">A list of Collections Databases</param>
        /// <returns></returns>
        public static List<string> FindRoundsWithoutItineraries(List<InCabDB> InCabClients, List<CollectionsDB> CollectionsClients)
        {
            // Derive a list of Collections Databases for which there are matching InCab Databases   (Essentially doing an INNER JOIN on both lists over the URLName attribute)
            List<CollectionsDB> ClientsWithBoth = CollectionsClients.Where(x => InCabClients.Any(y => x.URLName == y.URLName)).ToList();

            // Define an empty list of errors to populate
            List<string> listItinNotFound = new List<string>();

            foreach (CollectionsDB server in ClientsWithBoth)
            {
                // Derive a list of each distinct Itinerary in the InCabDB by concatenating Day/Week/Service/Crew
                Console.WriteLine("Finding itineraries for " + server.Name);
                List<string> inCabItineraries = GetItineraries(server);

                // Derive a list of InCab Services from the ColClient table
                List<string> incabServices = server.RunScalarQuery(new SqlCommand("SELECT TOP 1 inCabServices FROM colClient"), "Collections").Split('#').ToList();

                // Derive rounds for each service and check for their existence in inCabItineraries
                foreach (string service in incabServices)
                {
                    Console.WriteLine("Finding itineraries for " + server.Name + " (" + service + ")");
                    List<string> roundsForThisService = parseRounds(server.getRoundsForService(service));

                    foreach (string round in roundsForThisService)
                    {
                        // Add the Round to the error log if it wasn't in inCabItineraries
                        if (!inCabItineraries.Contains(round))
                            listItinNotFound.Add(server.Name + " # " + round);
                    }
                }
            }
            return listItinNotFound;
        }

        /// <summary>
        /// Parses rounds from WaxRoundNew format into InCab Itinerary format
        /// </summary>
        /// <param name="rawRounds"></param>
        /// <returns></returns>
        public static List<string> parseRounds(List<string> rawRounds)
        {
            List<string> parsedRounds = new List<string>();
            foreach (string round in rawRounds)
            {
                string[] roundname = round.Split(' ');
                int rl = roundname.Length;
                try
                {
                    parsedRounds.Add(string.Format("{0} {1} {2} {3}", roundname[rl - 4], roundname[rl - 3], roundname[rl - 2].Replace("Wkly", "Wk1"), roundname[rl - 1]));
                }
                catch
                {
                    parsedRounds.Add(roundname + "-!!!Invalid Format!!!");
                }
            }
            return parsedRounds;
        }

        /// <summary>
        /// Gets distinct Itineraries from the InCabItineraries table and formats them into InCab format
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        private static List<string> GetItineraries(CollectionsDB server)
        {
            DataTable itineraries = server.RunQuery(new SqlCommand("SELECT distinct itineraryRound,itineraryDay,itineraryWeek,itineraryService FROM InCabItinerary"), "InCab");

            List<string> inCabItineraries = new List<string>();
            foreach (DataRow row in itineraries.Rows)
            {
                inCabItineraries.Add("Cr" + row["itineraryRound"].ToString() + " " +
                    row["itineraryDay"].ToString() + " " +
                    ConvertInCabWeekToCollections(row["itineraryWeek"].ToString()) + " " +
                    GetAbbrServiceName(row["itineraryService"].ToString()));
            }

            return inCabItineraries;
        }

        /// <summary>
        /// Converts a string containing just numbers into Collections Week Format.
        /// Returns the input unmodified if input was not just a number.
        /// </summary>
        /// <param name="weekAbbreviation"></param>
        /// <returns></returns>
        private static string ConvertInCabWeekToCollections(string itineraryWeek)
        {
            string WeekNumber = new string(itineraryWeek.Where(char.IsDigit).ToArray());
            if (WeekNumber.Length > 0)
                return "Wk" + WeekNumber;
            return itineraryWeek;
        }

        private static string GetAbbrServiceName(string ServiceAbbreviation)
        {
            switch (ServiceAbbreviation)
            {
                case "Refuse":
                    return "Ref";
                case "Recycling":
                    return "Rec";
                case "Garden":
                    return "Grn";
                case "Glass":
                    return "Gls";
                case "TradeRefuse":
                    return "TRf";
                case "Trade":
                    return "TRf";
                case "Plastic":
                    return "Pls";
                case "Food":
                    return "Foo";
                case "Box":
                    return "Box";
                case "Organic":
                    return "Grn";

                default:
                    return ServiceAbbreviation;
            }
        }
    }
}
