﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DevOpsConsole.ScheduledTasks
{
    class WMDPerformanceTidier : ScheduledTask
    {
        /// <summary>
        /// Set the attributes of the parent ScheduledTask 
        /// according to this specific routine
        /// </summary>
        public WMDPerformanceTidier()
        {
            // this is how the task is identified in the Jobhistory Table and 
            // also its run command in the console
            name = "WMDPerformance Tidier";

            // This is how often the task needs to run (must be in hours)
            // The task scheduler will run a task if it hasn't been run within 75% of its frequency
            // i.e. if a task has frequency 4 hours and hasn't run within the last 3 hours 
            frequency = TimeSpan.FromHours(ReadFrequency());

            recipients = ReadRecipients();
        }

        /// <summary>
        /// This is the actual business logic of what the task does.
        /// </summary>
        public override void RunTaskInnerLogic()
        {
            Console.WriteLine("Tidying the WMDPerformance table...");

            // Gather a list of all Collections databases
            List<CollectionsDB> CollectionsClients = getAllColDBs();

            foreach (CollectionsDB colDB in CollectionsClients)
            {
                // Remove duplicate rows from each database's WMDPerformance table
                DeleteWMDPerformanceDuplicates(colDB);
            }
            Console.WriteLine("Finished tidying performance table");
        }

        /// <summary>
        /// Remove duplicate rows from a database's WMDPerformance table
        /// </summary>
        /// <param name="client"></param>
        public static void DeleteWMDPerformanceDuplicates(CollectionsDB client)
        {
            Console.WriteLine("Checking for Duplicate Rows in WMDPerformance for " + client.displayName);

            // Load in a query to discover duplicated rows
            var ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("DevOpsConsole.dll", "");

            SqlCommand GetWMDPerformanceDuplicates = new SqlCommand(File.ReadAllText(Path.Combine(ApplicationPath, "Queries", "GetWMDPerformanceDuplicates.sql")));

            // Gather a datatable of duplicated rows
            DataTable WMDPerformanceDuplicates = client.RunQuery(GetWMDPerformanceDuplicates, "Collections");

            // If there are no duplicates, end here
            if (WMDPerformanceDuplicates == null)
                return;

            // Pronounce and delete each duplicate
            foreach (DataRow row in WMDPerformanceDuplicates.Rows)
            {
                SqlCommand DeleteDuplicate = new SqlCommand("DELETE FROM WMDPerformance WHERE ID = " + row["ID"].ToString());
                Console.WriteLine("Deleting row with ID: " + row["ID"].ToString());
                client.RunNonQuery(DeleteDuplicate, "Collections");
            }
            Console.WriteLine("Done");
        }
    }
}
