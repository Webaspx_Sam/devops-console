﻿WITH WaitingJobs AS (
	SELECT 
		[ID], 
		[Status],
		CONVERT(DATETIME, SUBSTRING(Results, 0, CHARINDEX('#', Results, 0)), 103) AS 'DateChanged'
	FROM 
		[WMDPerformance] 
	WHERE 
		[Status] = 'Waiting'
)

SELECT 
	COUNT(*) 
FROM 
	WaitingJobs 
WHERE 
	(DateChanged < DATEADD(MINUTE, -30, GETDATE()))