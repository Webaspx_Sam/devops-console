﻿SELECT 
Bins.Bin_number, 
COUNT(Bin_number) AS Properties 
FROM (
	SELECT Bin_number FROM Bins
) AS Bins 
WHERE Bins.Bin_number NOT IN (
	SELECT DISTINCT BinType FROM BinShop
)
GROUP BY Bin_number