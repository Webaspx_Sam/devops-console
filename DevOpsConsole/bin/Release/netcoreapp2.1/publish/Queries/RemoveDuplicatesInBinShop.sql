﻿WITH dupFinder AS (
	SELECT 
	BinType,
	ROW_NUMBER() OVER (
		PARTITION BY 
			BinType 
		ORDER BY 
			BinType
		) row_num 
	FROM BinShop
)

DELETE FROM dupFinder WHERE row_num > 1;