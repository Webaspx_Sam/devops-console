﻿SELECT 
CollectionID,
UPRN,
Day_DDD,
[Week],
ServiceName,
Crew,
RoundName,
ROW_NUMBER() OVER (
	PARTITION BY 
		UPRN, 
		Day_DDD,
		[Week],
		ServiceName,
		Crew
	ORDER BY
		UPRN,
		Day_DDD,
		[Week],
		ServiceName,
		Crew
	) row_num
FROM WaxRoundNew
WHERE RoundName NOT LIKE 'Admin%' 
ORDER BY row_num DESC