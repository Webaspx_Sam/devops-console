﻿SELECT 
Rounds.RoundName, 
COUNT(RoundName) AS Properties 
FROM (
	SELECT RoundName FROM WaxRoundNew
) AS Rounds 
WHERE Rounds.RoundName NOT IN (
	SELECT DISTINCT RoundName FROM RoundShop
)
GROUP BY RoundName