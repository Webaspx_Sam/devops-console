﻿IF EXISTS (SELECT TOP 1 collectsOnASun FROM colClient WHERE collectsOnASun = 'Y')

SELECT 
[11] as temp,
UPRN,
[1] as Round1,
[2] as Round2,
[3] as Round3,
[4] as Round4, 
[5] as Round5,
[6] as Round6, 
[7] as Round7,
[8] as Round8, 
[9] as Round9,
[10] as Round10 
FROM (
	SELECT 
	UPRN,
	RoundName, 
	Row_Number() Over (
		Partition By UPRN 
		Order By UPRN) as RowNum 
	FROM WaxRoundNew 
	WHERE UPRN IN (
		select UPRN 
		from LLPG)
	AND roundname NOT LIKE 'Admin%' 
	AND roundname NOT LIKE '%TRf') 
a PIVOT (Max(RoundName) FOR RowNum in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11])) as pvt

ELSE 

SELECT 
[11] as temp,
UPRN,
[1] as Round1,
[2] as Round2,
[3] as Round3,
[4] as Round4, 
[5] as Round5,
[6] as Round6, 
[7] as Round7,
[8] as Round8, 
[9] as Round9,
[10] as Round10 
FROM (
	SELECT 
	UPRN,
	RoundName, 
	Row_Number() Over (
		Partition By UPRN 
		Order By UPRN) as RowNum 
	FROM WaxRoundNew 
	WHERE UPRN IN (
		select UPRN 
		from LLPG)
	AND roundname NOT LIKE '%Sun Wk%' 
	AND roundname NOT LIKE '%TRf') 
a PIVOT (Max(RoundName) FOR RowNum in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11])) as pvt