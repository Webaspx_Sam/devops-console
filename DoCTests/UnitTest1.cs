using System;
using System.Collections.Generic;
using NUnit.Framework;
using DevOpsConsole;
using DevOpsConsole.ScheduledTasks;

namespace AzureMonitorTests
{
    [TestFixture]
    public class validateRoundNameTests
    {
        [TestCase("Cr99 Mon Wk2 Ref", true, TestName = "Valid Input")]
        [TestCase("Admin Cr99 Mon Wk2 Ref", true, TestName = "Valid Input with Admin")]
        [TestCase("Redcoats Cr99 Mon Wk2 Ref", true, TestName = "Valid Input with prepended description")]
        [TestCase("Admin Bad logical status Cr99 Sun Wk1 Rec", true, TestName = "Valid Input with many prepended descriptions")]
        [TestCase("CrA Mon Wk2 Ref", true, TestName = "Valid Input with Lettered Crew")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null String")]
        [TestCase("frog", false, TestName = "Input too short")]
        [TestCase("AnythingCr99 Mon Wk2 Ref", false, TestName = "Prepended string has no space")]
        [TestCase("CrZ4 Mon Wk2 Ref", false, TestName = "Double Lettered Crew")]
        [TestCase("Cr99 Mon Wk2 Ref ", false, TestName = "Trailing space")]
        [TestCase(" Cr99 Mon Wk2 Ref ", false, TestName = "Leading space")]
        [TestCase("Cr99   Mon Wk2 Ref", false, TestName = "Multiple spaces 1")]
        [TestCase("Cr99 Mon   Wk2 Ref", false, TestName = "Multiple spaces 2")]
        [TestCase("Cr99 Mon Wk2   Ref", false, TestName = "Multiple spaces 3")]
        [TestCase("Cr99 \rMon \nWk2 Ref", false, TestName = "Escape characters")]
        [TestCase("Cr99 Wk2 Mon Ref", false, TestName = "Wrong order")]
        [TestCase("frog Wk2 Mon Ref", false, TestName = "Wrong Crew")]
        [TestCase("Cr99 bat Mon Ref", false, TestName = "Wrong Week")]
        [TestCase("Cr99 Wk2 cat Ref", false, TestName = "Wrong Day")]
        [TestCase("Cr99 Wk2 Mon dog", false, TestName = "Wrong Service")]
        [TestCase("Cr99 Wk2 Mon dog", false, TestName = "Wrong Service")]
        [TestCase("Cr0 Mon Wk2 Ref", false, TestName = "Crew Number too low")]
        [TestCase("Cr100 Mon Wk2 Ref", false, TestName = "Crew Number too high")]
        [TestCase("Cr99 Mon Wk0 Ref", false, TestName = "Week Number too low")]
        [TestCase("Cr99 Mon Wk10 Ref", true, TestName = "Week double digits")]
        [TestCase("Cr99 Mon Wk54 Ref", false, TestName = "Week too high")]
        [TestCase("Cra Mon Wk2 Ref", false, TestName = "Lower case Crew")]
        [TestCase("CrA Mon Wk2 Ref", true, TestName = "Upper case Crew")]
        [TestCase("Admin$ Cr99 Mon Wk2 Ref", false, TestName = "Contains Hash Symbol")]
        public void validateRoundnameTest(string Roundname, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateRoundname(Roundname));
        }
    }

    [TestFixture]
    public class validateCollectionsCrewTests
    {
        [TestCase("Cr42", true, TestName = "Valid Input")]
        [TestCase("CrA", true, TestName = "Valid Input2")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("notavalid input", false, TestName = "Invalid Input")]
        [TestCase("Cr0", false, TestName = "Crew Number too low")]
        [TestCase("CrAA", false, TestName = "Invalid Input2")]
        [TestCase("Cr100", false, TestName = "Crew Number too high")]

        public void validateCrewTest(string Crew, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateCollectionsCrew(Crew));
        }
    }

    [TestFixture]
    public class validateDayTests
    {
        [TestCase("Mon", true, TestName = "Valid Input")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("notavalid input", false, TestName = "Invalid Input")]

        public void validateCrewTest(string Day, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateDay(Day));
        }
    }

    [TestFixture]
    public class validateWeekTests
    {
        [TestCase("Wk1", true, TestName = "Valid Input")]
        [TestCase("Wkly", true, TestName = "Valid Input")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("notavalid input", false, TestName = "Invalid Input")]
        [TestCase("Wk0", false, TestName = "Week Number too low")]
        [TestCase("Wk53", true, TestName = "Week Double Digits")]
        [TestCase("Wk54", false, TestName = "Week Number too high")]

        public void validateWeekTest(string Week, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateWeek(Week));
        }
    }

    [TestFixture]
    public class validateCollectionsServiceTests
    {
        [TestCase("Ref", true, TestName = "Valid Input")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("notavalid input", false, TestName = "Invalid Input")]
        [TestCase("Refuse", false, TestName = "Incab Format")]

        public void validateCollectionsServiceTest(string Service, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateCollectionsService(Service));
        }
    }

    [TestFixture]
    public class validateUPRNTests
    {
        [TestCase("658273894", true, TestName = "Valid Input")]
        [TestCase("10002491005", true, TestName = "Valid Input 2")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("notavalid input", false, TestName = "Invalid Input")]
        [TestCase("1E+11", false, TestName = "Standard Form (Excel)")]

        public void validateUPRNTest(string UPRN, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateUPRN(UPRN));
        }
    }

    [TestFixture]
    public class validateInCabServiceTests
    {
        [TestCase("Refuse", true, TestName = "Valid Input")]
        [TestCase("refuse", true, TestName = "Valid Input 2")]
        [TestCase("ORgaNIC", true, TestName = "Valid Input 3")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("recycle", false, TestName = "Invalid Input")]
        [TestCase("Rec", false, TestName = "Collections Format")]

        public void validateInCabServiceTest(string Service, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateInCabService(Service));
        }
    }

    [TestFixture]
    public class validateInCabCrewTests
    {
        [TestCase("1", true, TestName = "Valid Input")]
        [TestCase("0", true, TestName = "Valid Input 2")]
        [TestCase("99", true, TestName = "Valid Input 3")]
        [TestCase("A", true, TestName = "Valid Input 3")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("-1", false, TestName = "Invalid Input")]
        [TestCase("word", false, TestName = "Invalid Input 2")]
        [TestCase("100", false, TestName = "Invalid Input 3")]
        [TestCase("AA", false, TestName = "Invalid Input 3")]
        [TestCase("a", false, TestName = "Invalid Input 3")]

        public void validateInCabCrewTest(string Crew, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateInCabCrew(Crew));
        }
    }

    [TestFixture]
    public class validateInCabWeekTests
    {
        [TestCase("Week 2", true, TestName = "Valid Input")]
        [TestCase("week53", true, TestName = "Valid Input 2")]
        [TestCase("WeEK1", true, TestName = "Valid Input 3")]
        [TestCase("", false, TestName = "Empty String")]
        [TestCase(null, false, TestName = "Null Input")]
        [TestCase("Week  2", false, TestName = "Invalid Input")]
        [TestCase("Week 0", false, TestName = "Invalid Input 2")]
        [TestCase("week54", false, TestName = "Invalid Input 2")]

        public void validateInCabWeekTest(string Week, bool result)
        {
            DBValidator validator = new DBValidator();
            Assert.That(result == validator.validateInCabWeek(Week));
        }
    }

    //[TestFixture]
    //public class getRoundnameComponentsTests
    //{
    //    [TestCase("Cr99 Mon Wk1 Ref", new List<string>() { "Cr99", "Mon", "Wk1", "Ref" }, TestName = "Valid Input")]
    //    [TestCase("", false, TestName = "Empty String")]
    //    [TestCase(null, false, TestName = "Null Input")]
    //    [TestCase("notavalid input", false, TestName = "Invalid Input")]
    //    [TestCase("Cr0", false, TestName = "Crew Number too low")]
    //    [TestCase("Cr100", false, TestName = "Crew Number too high")]

    //    public void getRoundnameComponentsTest(string Roundname, List<String> result)
    //    {
    //        DBValidator validator = new DBValidator();
    //        Assert.That(result == getRoundnameComponents(Roundname));
    //    }
    //}
}