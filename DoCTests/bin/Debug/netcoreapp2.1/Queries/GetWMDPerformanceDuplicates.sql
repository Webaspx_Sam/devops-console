﻿/****** Script for SelectTopNRows command from SSMS  ******/

WITH dupFinder AS (
	SELECT  
		   [ID]
		  ,[Council]
		  ,[Date]
		  ,[ServiceName]
		  ,[RoundName]
		  ,[Day]
		  ,[Week]
		  ,[CreatedBy]
		  ,[JobType]
		  ,[Status]
		  --,[Results]
		  ,[ExtraData]
		  ,[JobPriority]
		  ,ROW_NUMBER() OVER (
	PARTITION BY 
		   [Council]
		  ,[Date]
		  ,[ServiceName]
		  ,[RoundName]
		  ,[Day]
		  ,[Week]
		  ,[CreatedBy]
		  ,[JobType]
		  ,[Status]
		  ,[Results]
		  ,[ExtraData]
		  ,[JobPriority]
	ORDER BY 
		   [Council]
		  ,[Date]
		  ,[ServiceName]
		  ,[RoundName]
		  ,[Day]
		  ,[Week]
		  ,[CreatedBy]
		  ,[JobType]
		  ,[Status]
		  ,[Results]
		  ,[ExtraData]
		  ,[JobPriority]
			) row_num 
	FROM WMDPerformance 
	)
SELECT * FROM dupFinder WHERE row_num > 1