﻿SELECT 
	JobStatus, 
	JobCrew, 
	JobDay, 
	JobWeek, 
	JobService, 
	JobItemUPRN, 
	JobItemAddress, 
	JobItemStreet, 
	JobItemTown, 
	JobItemPostcode, 
	JobItemEasting, 
	JobItemNorthing, 
	JobItemLocality, 
	JobType, 
	WorkflowCases.CreationDate,
	[Type], 
	JobDate, 
	JobID, 
	WorkflowCases.[Priority] 
FROM JobList 
INNER JOIN WorkflowTasks on JobList.JobID = WorkflowTasks.ID 
INNER JOIN WorkflowCases on WorkflowTasks.CaseID = WorkflowCases.CaseID 
WHERE [Type] = 'cop_bulky' 
	AND JobStatus != 'cancelled' 
	AND JobStatus != 'Issue'
	AND JobStatus != 'Completed'
	AND JobStatus != 'closed' 
	AND JobStatus != 'closed' 
	AND JobID != -1 
	AND JobDate = (
		SELECT TOP 1 JobDate 
		FROM JobList 
		WHERE JobStatus = 'waiting' 
		AND [Type] = 'cop_bulky' 
		AND JobID != -1
		)
ORDER BY JobDate ASC;