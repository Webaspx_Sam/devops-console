﻿With dupFinder AS (
	SELECT 
	CollectionID,
	UPRN, 
	RoundName,
	ROW_NUMBER() OVER (
		PARTITION BY 
			UPRN,
			RoundName
		ORDER BY
			UPRN,
			RoundName
		) row_num
	FROM WaxRoundNew
	WHERE RoundName NOT LIKE 'Admin%'
)

DELETE FROM dupFinder WHERE row_num > 1;