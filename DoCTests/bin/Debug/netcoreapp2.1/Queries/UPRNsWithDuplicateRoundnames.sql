﻿	SELECT 
	CollectionID,
	UPRN, 
	RoundName,
	ROW_NUMBER() OVER (
		PARTITION BY 
			UPRN,
			RoundName
		ORDER BY
			UPRN,
			RoundName
		) row_num
	FROM WaxRoundNew
	WHERE RoundName NOT LIKE 'Admin%'
	ORDER BY row_num DESC